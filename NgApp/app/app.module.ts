import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { LocalStorageModule } from 'angular-2-local-storage';
import { AppComponent }   from './app.component';
import { DiagnosticsComponent } from './components/diagnostics.component';
import { StationComponent } from './components/station.component';
import { StationsComponent } from './components/stations.component';
import { SearchComponent } from './components/search.component';
import { AqiGaugeComponent } from './components/aqi-gauge.component';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {AqiGaugeWidthService} from './components/aqi-gauge-width.service';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { FormsModule }   from '@angular/forms';
import { GeoLocationService} from './components/geo-location.service';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MapComponent } from './components/map.component';
import { MyStationsComponent } from './components/mystations.component';
import { MyStationsService} from './components/mystations.service';
import { StationDetailsComponent } from './components/station-details.component';

const appRoutes: Routes = [
  {
    path: 'search',
    component: SearchComponent,
    data: {
      title: 'Search'
    }
  },
  {
    path: 'map',
    component: MapComponent,
    data: {
      title: 'Map'
    }
  },
  {
    path: 'mystations',
    component: MyStationsComponent,
    canActivate: [MyStationsService],
    data: {
      title: 'My stations'
    }
  },
  {
    path: 'station:key',
    component: StationDetailsComponent,
    data: {
      title: 'Station details'
    }
  },
  { path: '',   redirectTo: '/mystations', pathMatch: 'full',  canActivate: [MyStationsService] },
  //{ path: '**', component: AppComponent }
];

@NgModule({
  imports:      [ BrowserModule, FormsModule,HttpModule,JsonpModule,NgbModule.forRoot(),RouterModule.forRoot(appRoutes), 
                  InfiniteScrollModule, AgmCoreModule.forRoot({apiKey: 'AIzaSyDaaDuRVY_XJle241Z2q9U-UqaFlbSOA5Y'}),
                  LocalStorageModule.withConfig({prefix: 'smog-app',storageType: 'localStorage'}) ],
  declarations: [ AppComponent,DiagnosticsComponent,StationComponent,StationsComponent,SearchComponent,AqiGaugeComponent, 
    MapComponent, MyStationsComponent, StationDetailsComponent],
  bootstrap:    [ AppComponent ],
  providers:    [AqiGaugeWidthService, { provide: 'Window',  useValue: window },GeoLocationService,MyStationsService]
})
export class AppModule { }
