import { Injectable }     from '@angular/core';
import { LocalStorageService  } from 'angular-2-local-storage';
import { CanActivate }    from '@angular/router';
import * as _ from "lodash";

// import { Http, Response,URLSearchParams } from '@angular/http';
// import { StationViewModel }    from './station.models';
// import { Observable }     from 'rxjs/Rx';

// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';

@Injectable()
export class MyStationsService implements CanActivate  {


  constructor ( private localStorageService: LocalStorageService) {

  }
  
  public canActivate()
  {
      return this.localStorageService.isSupported;
  }
  
  public addMyStation(stationKey: string) : void
  {
    this.localStorageService.set(stationKey, true);
  }

  public removeMyStation(stationKey: string) : void
  {
    this.localStorageService.remove(stationKey);
  }

  public getMyStations() : string[]
  {
    return this.localStorageService.keys();
  }

  public isMyStation(stationKey: string) : boolean
  {
    return this.localStorageService.get<boolean>(stationKey);
  }
}
