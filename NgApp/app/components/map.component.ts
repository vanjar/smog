import { Component, Input,ViewChild,ChangeDetectionStrategy } from '@angular/core';
import { StationViewModel} from './station.models';
import {GeoLocationService} from './geo-location.service';
import { CoordinateRange} from './map.model';
import {StationsService} from './stations.service';
import * as _ from 'lodash';

@Component({
    moduleId: module.id,
    selector: 'map',
    templateUrl: 'map.component.html',
    providers: [GeoLocationService, StationsService],
    //changeDetection: ChangeDetectionStrategy.OnPush
 })


export class MapComponent  { 
    //@Input() station: StationViewModel;
    //@ViewChild('aqiInfoContainer') aqiInfoContainer;
    lat: number;
    lng: number;
    coord: any;
    latRange: CoordinateRange;
    lngRange: CoordinateRange;
    stationViewModels: StationViewModel[];

    constructor (private geoLocationService : GeoLocationService, private stationsService: StationsService)
    {
      
    }

     ngOnInit() {
         this.geoLocationService.getLocation(null).subscribe(pos=>{
            this.lng =pos.coords.longitude;
            this.lat = pos.coords.latitude;
            console.log('oninit finished')
        },
         err=>{this.lng = null; this.lat = null; console.log(err);});
    }

    boundsChanged(event: any)
    {
        this.latRange  = new CoordinateRange(Number(event.f.f), Number(event.f.b));
        this.lngRange = new CoordinateRange(Number(event.b.b), Number(event.b.f));
        console.log('boundChanged')
    }
  
    getStationIcon(station: StationViewModel)
    {
        if (station.dominantAqiPolutant == null)
            return "../img/map-aqi-null.png";
        return "../img/map-aqi-"+station.dominantAqiPolutant.category+".png";
    }

    idle()
    {
        this.stationsService.mapSearch(this.latRange.from, this.latRange.to, this.lngRange.from, this.lngRange.to, this.lat,this.lng)  
            .subscribe(stations=>{
                if (this.stationViewModels == null){
                    this.stationViewModels = stations;
                    return;
                }
                    let toAdd = _.filter(stations, (s)=> _.find(this.stationViewModels, (vm)=> vm.stationInfo.key == s.stationInfo.key) == null);
                    let toDelete =  _.filter(this.stationViewModels, (vm)=> _.find(stations, (s)=> vm.stationInfo.key == s.stationInfo.key) == null);
                    _.forEach(toDelete,(d)=> _.remove(this.stationViewModels, (vm)=> vm.stationInfo.key == d.stationInfo.key));
                   this.stationViewModels = _.union(this.stationViewModels, toAdd) 
            }, err=>{console.log(err)})
    }

    clicked(event: any)
    {
        console.log("lat: "+event.coords.lat + ' lng:'+event.coords.lng)
    }
}


