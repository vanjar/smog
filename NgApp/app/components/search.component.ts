import { Component,AfterViewInit,ViewChild } from '@angular/core';
import { StationViewModel} from './station.models';
import { StationsService } from './stations.service';
import {GeoLocationService} from './geo-location.service';

@Component({
    moduleId: module.id,
    selector: 'search',
    templateUrl: 'search.component.html',
    providers: [StationsService,GeoLocationService]
})
export class SearchComponent implements AfterViewInit  { 
    @ViewChild('searchInput') searchInput;
    searchResult: string[];
    searchPattern: string;
    lat: number;
    lng: number;

    constructor (private stationsService : StationsService,private geoLocationService : GeoLocationService)
    {
    }

    ngOnInit() {
      
         this.geoLocationService.getLocation(null).subscribe(pos=>{
            this.lng =pos.coords.longitude;
            this.lat = pos.coords.latitude;
        },
         err=>{this.lng = null; this.lat = null; console.log(err);});
    }

    submit($event: Event) {
        $event.preventDefault();
        this.search();
    }

    search()
    {
          this.stationsService.searchStations(this.searchPattern,this.lat,this.lng)  
            .subscribe(stationKeys=>{
                if (stationKeys == null || stationKeys.length == 0)
                {
                    this.searchResult = [];
                }
                else
                {
                    this.searchResult =stationKeys;
                }
            }, err=>{console.log(err)})
    }

    ngAfterViewInit() {
        this.searchInput.nativeElement.focus();
    }
}
