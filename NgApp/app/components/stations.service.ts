import { Injectable }     from '@angular/core';
import { Http, Response,URLSearchParams } from '@angular/http';
import { StationViewModel }    from './station.models';
import { Observable }     from 'rxjs/Rx';
import * as _ from "lodash";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class StationsService {

  private allStationsUrl = 'http://localhost:64694/station';  
  private stationsSearchUrl = "http://localhost:64694/station/search"
  private mapSearchUrl = "http://localhost:64694/station/mapsearch"
  private nearestUrl = "http://localhost:64694/station/nearest"
  //private stationUrl = 'http://localhost:64694/station';

  constructor (private http: Http) {

  }

  getLocation()
  {

  }

  getStations (stationKeys : string[], cols: number,lat: number, lng: number): Observable<StationViewModel[]> {
    let params: URLSearchParams = new URLSearchParams();
    params.append("cols",cols.toString());
    params.append("localDateTime",new Date().toISOString());
     if (lat != null)
      params.set("lat", lat.toString());
    if (lng != null)
      params.set("lng", lng.toString());
    _.forEach(stationKeys, (k)=>{params.append("key",k)})
    return this.http.get(this.allStationsUrl,{search: params})
                    .map(this.extractData)
                    .catch(this.handleError);
  }


  searchStations(searchPattern: string,lat: number, lng: number): Observable<string[]> {
     let params: URLSearchParams = new URLSearchParams();
    params.set("s", searchPattern);
    if (lat != null)
      params.set("lat", lat.toString());
    if (lng != null)
      params.set("lng", lng.toString());
    return this.http.get(this.stationsSearchUrl,{ search: params })
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  mapSearch(latFrom: number, latTo: number, lngFrom: number, lngTo:number, lat: number, lng: number): Observable<StationViewModel[]> {
     let params: URLSearchParams = new URLSearchParams();
    params.set("latFrom", latFrom.toString());
    params.set("lngFrom", lngFrom.toString());
    params.set("latTo", latTo.toString());
    params.set("lngTo", lngTo.toString());
    params.set("lat", lat.toString());
    params.set("lng", lng.toString());
    params.set("clientDateTime",new Date().toISOString());
    return this.http.get(this.mapSearchUrl,{ search: params })
                    .map(this.extractData)
                    .catch(this.handleError);
  }

 getNearest (lat: number, lng: number): Observable<StationViewModel> {
    let params: URLSearchParams = new URLSearchParams();
    params.append("clientDateTime",new Date().toISOString());
    params.set("lat", lat.toString());
    params.set("lng", lng.toString());
    return this.http.get(this.nearestUrl,{search: params})
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body;
    if (res.text())
    {
      body= res.json();
      //console.log(body);
    }
    else
    {
      console.log("Empty response")
    }
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
