import { AfterViewInit, Component, ViewChild, Input } from '@angular/core';
import * as d3 from 'd3';
import { AqiGaugeWidthService} from './aqi-gauge-width.service';

@Component({
    moduleId: module.id,
    selector: 'aqi-gauge',
    template: '<span #aqigauge class="aqi-gauge" touch-action="none"></span>',
})
export class AqiGaugeComponent implements AfterViewInit {
    @ViewChild('aqigauge') aqiGauge;
    @Input('aqi') aqiInput: number;
    // @Input('gaugeWidth') gaugeWidth: number;
    constructor(private aqiGaugeWidthService : AqiGaugeWidthService) {
    }

    scaleAqi(aqi: number, barwidth: number) {
        var offset = 0;
        var factor = barwidth / 50;
        if (aqi <= 0)
            return 0;

        if (aqi > 500)
            return barwidth * 6;

        if (aqi <= 200) {
            return aqi * factor;
        }

        if (aqi > 200 && aqi <= 300) {
            offset = 4 * barwidth;
            return offset + (aqi - 200) / 2 * factor
        }

        if (aqi > 300) {
            offset = 5 * barwidth;
            return offset + (aqi - 300) / 4 * factor
        }
    }

    getPointer(aqi: number, bwidth: number, bheight: number, offset: number) {
        var size = 10;
        var x3 = offset + this.scaleAqi(aqi, bwidth)
        var y3 = bheight-size/2;

        var x1 = x3 - (size / 2);
        var y1 = bheight + size / 3;

        var x2 = x1 + size;
        var y2 = y1;

        return "M " + x1 + " " + y1 + " "
            + "L " + x2 + " " + y2 + " "
            + "L " + x3 + " " + y3 + " "
            + "L " + x1 + " " + y1 + " ";
    }

    drawGauge() {
             //  console.time("drawGauge");
        let height = 10;
        let offset = 10;

        let svg = d3.select(this.aqiGauge.nativeElement)
            .append("svg:svg")
            .attr("width","100%")
            .attr("height", 28)
            .append("g");


        let width = this.aqiGaugeWidthService.getGaugeWidth();
        svg.selectAll()
            .data(d3.range(0,6,1))
            .enter()
            .append("rect")
            .attr("class", function (d, i) { return "aqi-" + (i + 1); })
            .attr("x", function (d, i) { return offset +  i * width })
            .attr("y", 12)
            .attr("width", width)
            .attr("height", height);

        svg.append("path")
            .attr("d", this.getPointer(this.aqiInput, width, height+12,offset))
            .attr("class", "pointer");

        svg.append("text")
            .attr("x",offset+ this.scaleAqi(this.aqiInput, width))
            .attr("y",10)
            .attr("font-size","0.8em")
            .attr("text-anchor","middle")
            .text(this.aqiInput);

        //console.timeEnd("drawGauge");
    }

    ngAfterViewInit() {
      
    }

    ngOnInit()
    {
        this.drawGauge();

    }
}


