import { Injectable } from '@angular/core';

@Injectable()
export class AqiGaugeWidthService {
  gaugeWidth:number;

  constructor()
  {}

     setGaugeWidth(val) {
        this.gaugeWidth = val;
    }

    getGaugeWidth() {
        return this.gaugeWidth;
    }
}