import { Component,Input, SimpleChange,ViewChild,Inject } from '@angular/core';
import { StationViewModel } from './station.models';
import { StationsService } from './stations.service';
import { GeoLocationService } from './geo-location.service';
import { MyStationsService } from './mystations.service';
import * as _ from 'lodash';

@Component({
    moduleId: module.id,
    selector: 'stations',
    templateUrl: 'stations.component.html',
    //providers: [StationsService,MyStationsService]
})
export class StationsComponent { 
    @Input() stationKeys: string[];
    @Input() lat: number;
    @Input() lng: number;

    @ViewChild('mqdetector') mqdetector;
    stationKeysToLoad: string[];
    stationViewModelBatches: StationViewModel[][];
    stationsToLoad: string[];
    initialized: boolean;
    batch: number;
    myStationsEnabled: boolean;

    constructor (private stationsService : StationsService,
    private geoLocationService: GeoLocationService, private myStationsService: MyStationsService,
        @Inject('Window') window: Window)
    {
        this.stationsToLoad = null;
        this.batch = 20;
        
        this.myStationsEnabled = myStationsService.canActivate();
    }

    onScroll()
    {
        console.log("On scroll;")
    //    this.loadStations();
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChange) {
          if(changes["stationKeys"] !== undefined)
        {
            let currVal =changes['stationKeys'].currentValue;
            if (currVal == null || currVal.length == 0)
            {
                this.stationsToLoad = null;
                return;
            }
            
            this.stationViewModelBatches =null;
            this.stationKeysToLoad = currVal;
            this.loadStations();
        }
    }

    getNumberOfColumns()
    {
        return window.getComputedStyle(this.mqdetector.nativeElement).display == "none" ? 1: 3;
    }

    loadStations()
    {
        if (!_.some(this.stationKeysToLoad))
         return;
        let toLoad = _.take(this.stationKeysToLoad,this.batch);
        this.stationKeysToLoad = _.drop(this.stationKeysToLoad,this.batch);
        console.log("loading: "+toLoad.length);
        console.log("remaining: "+this.stationKeysToLoad.length );
        this.stationsService.getStations(toLoad,this.getNumberOfColumns(),this.lat,this.lng)  
            .subscribe(stations=>{
                _.forEach(stations, (s)=>{s.isMy = this.myStationsService.isMyStation(s.stationInfo.key)});
                if (this.stationViewModelBatches == null)
                    this.stationViewModelBatches = [stations];
                else
                   this.stationViewModelBatches.push(stations);
         
            }, err=>{console.log(err)})
    }

    isMyStationChanged($event)
    {
        if ($event.isMy)
            this.myStationsService.addMyStation($event.stationInfo.key)
        else
           this.myStationsService.removeMyStation($event.stationInfo.key)
        console.log("here comes local stored save: "+$event.stationInfo.key)
    }
}


