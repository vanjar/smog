import { Component } from '@angular/core';
import { DataProviderInfoModel} from './diagnostics.models'
import { DiagnosticsService } from './diagnostics.service'

@Component({
    moduleId: module.id,
    selector: 'diagnostics',
    templateUrl: 'diagnostics.component.html',
    providers: [DiagnosticsService]
})
export class DiagnosticsComponent { 

    dataProviderInfos: DataProviderInfoModel[];

    constructor (private diagnosticsService : DiagnosticsService)
    {}

    ngOnInit() {
            // Load comments
            this.loadDataProviderInfos()
    }

    loadDataProviderInfos()
    {
        this.diagnosticsService.getDataProviderInfos()  
            .subscribe(infos=>{
               this.dataProviderInfos = infos;
            }, err=>{console.log(err)})
    }
}


