import { Component,AfterViewInit,ViewChild } from '@angular/core';
import { StationViewModel} from './station.models';
import { StationsService } from './stations.service';
import {GeoLocationService} from './geo-location.service';
import { MyStationsService} from './mystations.service';
import * as _ from 'lodash';

@Component({
    moduleId: module.id,
    selector: 'mystations',
    templateUrl: 'mystations.component.html',
    providers: [StationsService,GeoLocationService]
})
export class MyStationsComponent implements AfterViewInit  { 
    lat: number;
    lng: number;
    noneExists: boolean;
    nearest: StationViewModel;
    myStationsKeys: string[];

    constructor (private stationsService : StationsService,private geoLocationService : GeoLocationService,
    private myStationsService:  MyStationsService)
    {
        
    }

    ngOnInit() {
       this.noneExists = this.myStationsService.getMyStations().length ==0;
       this.getMyStations();
    }
    
    getMyStations()
    {
        if (this.noneExists)
        {
            this.geoLocationService.getLocation(null)
                .flatMap((pos)=>  this.stationsService.getNearest(pos.coords.latitude, pos.coords.longitude))
                .subscribe(data=>this.nearest = data, err=>{
                        this.nearest = null;
                        this.lat = null;
                        this.lng = null;
                        console.log(err);});
        }
        else
        {
            this.myStationsKeys = this.myStationsService.getMyStations();
        }
    }
 
    ngAfterViewInit() {
      //  this.getStations();
    }
}
