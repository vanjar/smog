import { AfterViewInit,Component, Input,ViewChild,Output,EventEmitter  } from '@angular/core';
import { StationViewModel} from './station.models';
import { AqiGaugeWidthService} from './aqi-gauge-width.service';


@Component({
    moduleId: module.id,
    selector: 'station',
    templateUrl: 'station.component.html'
 })
export class StationComponent implements AfterViewInit { 
    @Input() station: StationViewModel;
    @Input() myStationsEnabled: boolean;
    @ViewChild('aqiInfoContainer') aqiInfoContainer;
    @Output() isMyStationChanged = new EventEmitter();
    //gaugeWidth = 0;
    
    constructor (private aqiGaugeWidthService : AqiGaugeWidthService)
    {
       
    }

    ngOnInit() {
     //-40 aqi-polutant-name-width
     //-10 left offset of gauge
    //console.time("drawStation");
    
        if (this.aqiGaugeWidthService.getGaugeWidth() == null)
        {
        //   console.time("calculating gauge width");
            let gaugeWidth = Math.ceil((this.aqiInfoContainer.nativeElement.getBoundingClientRect().width-40-10)/7);
    //     console.log('gauge width calculated in station ngOnInit: '+gaugeWidth);
            this.aqiGaugeWidthService.setGaugeWidth(gaugeWidth);
        // console.timeEnd("calculating gauge width");
        }
    
    }
 
    ngAfterViewInit() {
      //console.timeEnd("drawStation");
    }

    addOrRemoveMy($event)
    {
        this.station.isMy = !this.station.isMy;
        this.isMyStationChanged.emit(this.station);
    }
}


