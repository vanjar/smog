export class DataProviderInfoModel {
  dataProviderInfoId: string;
  dataProviderId: string;
  start: Date;
  stop: Date;
  status: number;
  statusDescription: string;
  info: string;
}