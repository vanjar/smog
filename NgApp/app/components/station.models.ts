export class StationInfoViewModel {
    stationId: string;
    key: string;
    stationName: string;
    address : string;
    lon: number;
    lat: number;
    distance: number;
    distanceCaption: string;
}
export class ProviderInfoViewModel 
{
    providerName: string;
    providerInfo: string;
}

export class StationPolutantViewModel {
    type: number;
    name: string;
    unit: number;
    unitName: string;
    interval: number;
    intervalName: string;
    value: number;

}

export class StationAqiPolutantViewModel {
    category: number;
    categoryName: string;
    aqi: number;
    polutantInfo: StationPolutantViewModel;
}


export class StationViewModel {
    stationInfo: StationInfoViewModel;
    providerInfo: ProviderInfoViewModel;
    timestamp: Date;
    aqiCalculatorType: string;
    dominantAqiPolutant: StationAqiPolutantViewModel;
    aqiPolutants: StationAqiPolutantViewModel[];
    otherPolutants: StationPolutantViewModel[];
    isMy: boolean;
}

