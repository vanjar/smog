import { Component,HostListener } from '@angular/core';
import { DiagnosticsService } from './components/diagnostics.service'
import { MyStationsService } from './components/mystations.service'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
@Component({
  moduleId: module.id,
  selector: 'smog-app',
  templateUrl: 'app.component.html',
  providers: [DiagnosticsService]
})
export class AppComponent   { 
    // isScrolledDown = false;
    // @HostListener('window:scroll', ['$event'])
    // track(event) {
    //         this.isScrolledDown =  (window.pageYOffset || document.documentElement.scrollTop) > 0;
    //         console.log(this.isScrolledDown);
    // }

    myStationsEnabled : boolean;

    constructor(private myStationsService: MyStationsService) {

    }

    ngOnInit() {
      
      this.myStationsEnabled = this.myStationsService.canActivate();
    }
}
