var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
    gulp.src('./styles/sass/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./styles/css/'))
});

gulp.task('default', ['sass'], function() {
    gulp.watch('./styles/sass/*.scss', ['sass']);
})