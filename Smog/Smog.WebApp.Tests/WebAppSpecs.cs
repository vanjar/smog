﻿using Autofac;
using Autofac.Core;
using Smog.Infrastructure;
using Smog.Infrastructure.Cache;
using Smog.WebApp.Controllers.Station;

namespace Smog.WebApp.Tests
{
    public class WebAppSpecs
    {
        protected ViewModelFactory ViewModelFactory;
        protected static IContainer Container;
        protected SmogCache SmogCache;

        public WebAppSpecs()
        {

            var builder = new ContainerBuilder();
            builder.RegisterModule<InfrastructureModule>();
            builder.RegisterModule<WebAppModule>();
           
            Container = builder.Build();
            ViewModelFactory = Container.Resolve<ViewModelFactory>();
            SmogCache = Container.Resolve<SmogCache>();
            SmogCache.Clear();
        }

 
    }
}
