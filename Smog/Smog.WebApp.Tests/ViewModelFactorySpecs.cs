﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using GeoCoordinatePortable;
using Smog.Infrastructure.Model;
using Smog.Infrastructure.Model.Enums;
using Smog.WebApp.Controllers.Station;
using Xunit;

// ReSharper disable InconsistentNaming
namespace Smog.WebApp.Tests
{
    
    public class when_building_station_info_view_model : WebAppSpecs
    {
         private readonly StationGeoDto _stationGeoDto 
            = new StationGeoDto(10,20,"stationId2", "providerId2", "street","streetNo","city","postalNo",
                "state","country","fromattedAdress",DateTimeOffset.Now);

        private readonly SmogDto _smogDto = new SmogDto(DateTime.Now.AddHours(-1),DateTime.Now,"stationName", "stationId2", 10,20,300,"country",
            "providerId2", "rsphsh",DateTimeOffset.Now.AddHours(-1),DateTimeOffset.Now, "slo",null);

        private readonly StationInfoViewModel _stationInfoViewModel;

        public when_building_station_info_view_model()
        {
            SmogCache.UpdateStationGeoData(_smogDto.Key, _stationGeoDto);
            _stationInfoViewModel = ViewModelFactory.CreateStationInfoViewModel(_smogDto, new GeoCoordinate(11, 21));
        }

        [Fact]
        public void it_should_build_station_info_view_model()
        {
            _stationInfoViewModel.ProviderId.Should().Be("providerId2");
            _stationInfoViewModel.StationId.Should().Be("stationId2");
            _stationInfoViewModel.Key.Should().Be("stationId2_providerId2");
            _stationInfoViewModel.Address.Should().Be("street streetNo, postalNo city, country");
            _stationInfoViewModel.Country.Should().Be("country");
            _stationInfoViewModel.City.Should().Be("city");
            _stationInfoViewModel.Distance.Should().Be(1546299.6570520261);
            _stationInfoViewModel.DistanceCaption.Should().Be("1546 km");
            _stationInfoViewModel.Lat.Should().Be(20);
            _stationInfoViewModel.Lon.Should().Be(10);
            _stationInfoViewModel.PostalNo.Should().Be("postalNo");
            _stationInfoViewModel.Street.Should().Be("street");
            _stationInfoViewModel.StreetNo.Should().Be("streetNo");
            _stationInfoViewModel.StationName.Should().Be("stationName");
        }
    }

    public class when_building_station_info_view_model_and_geo_data_does_not_exists_in_cache : WebAppSpecs
    {

        private readonly SmogDto _smogDto = new SmogDto(DateTime.Now.AddHours(-1), DateTime.Now, "stationName", "stationId", 10, 20, 300, "country",
            "providerId", "rsphsh", DateTimeOffset.Now.AddHours(-1), DateTimeOffset.Now, "slo", null);

        private readonly StationInfoViewModel _stationInfoViewModel;

        public when_building_station_info_view_model_and_geo_data_does_not_exists_in_cache()
        {
          _stationInfoViewModel = ViewModelFactory.CreateStationInfoViewModel(_smogDto, null);
        }
        
        [Fact]
        public void it_should_not_set_geo_data()
        {
            _stationInfoViewModel.Distance.Should().NotHaveValue();
            _stationInfoViewModel.DistanceCaption.Should().BeNull();
        }
    }

    public class when_building_station_info_view_model_and_client_coordinates_are_not_set : WebAppSpecs
    {
        private readonly StationGeoDto _stationGeoDto
            = new StationGeoDto(10, 20, "stationId", "provdierId", "street", "streetNo", "city", "postalNo",
                "state", "country", "fromattedAdress", DateTimeOffset.Now);

        private readonly SmogDto _smogDto = new SmogDto(DateTime.Now.AddHours(-1), DateTime.Now, "stationName", "stationId", 10, 20, 300, "country",
            "providerId", "rsphsh", DateTimeOffset.Now.AddHours(-1), DateTimeOffset.Now, "slo", null);

        private readonly StationInfoViewModel _stationInfoViewModel;

        public when_building_station_info_view_model_and_client_coordinates_are_not_set()
        {
            SmogCache.UpdateStationGeoData(_smogDto.Key, _stationGeoDto);
            _stationInfoViewModel = ViewModelFactory.CreateStationInfoViewModel(_smogDto, null);
        }

        [Fact]
        public void it_should_not_set_geo_data_in_relation_to_client_coordinates()
        {
            _stationInfoViewModel.Should().NotBeNull();
        }
    }

    public class when_building_station_view_model : WebAppSpecs
    {
        private readonly StationViewModel _stationViewModel;


        private SmogDto[] GetSmogDtos()
        {
            var fetchTime = DateTime.Now.AddHours(-1);
            var createdTime = DateTime.Now.AddHours(-2);
            return  Enumerable.Range(0,24)
                .Select(i=> new SmogDto(fetchTime, createdTime, "stationName", "stationId", 10, 20, 30, "Slovenia", "providerId", "hash",
                new DateTimeOffset(DateTime.Today.AddHours(i)),new DateTimeOffset(DateTime.Today.AddHours(i+1)),"si-Sl", 
                Enumerable.Range(0, Enum.GetNames(typeof(SmogPolutantType)).Length).Select(p=> 
                new SmogPolutantDto(p,(SmogPolutantType)p, (SmogPolutantInterval)(p%3),((SmogPolutantType)p) == SmogPolutantType.CO ? SmogPolutantUnit.MiliGramsPerCubicMetre : SmogPolutantUnit.MikroGramsPerCubicMetre)).ToArray()))
                .ToArray();
        }

        
        public when_building_station_view_model()
        {
            var smogDtos = GetSmogDtos();
            var stationGeoDto = new StationGeoDto(10, 20, "stationId", "provdierId", "street", "streetNo", "city", "postalNo",
           "state", "country", "fromattedAdress", DateTimeOffset.Now);
         SmogCache.UpdateStationGeoData(smogDtos[0].Key, stationGeoDto);
            _stationViewModel = ViewModelFactory.CreateStationViewModel(GetSmogDtos(), new GeoCoordinate(11, 21),DateTimeOffset.Now);
        }

        [Fact]
        public void it_should_build_station_view_model()
        {
            _stationViewModel.Should().NotBeNull();
            _stationViewModel.AqiCalculatorType.Should().Be("USEPA");
            _stationViewModel.AqiPolutants.Length.Should().BeGreaterThan(0);
            _stationViewModel.DominantAqiPolutant.Should().NotBeNull();
            _stationViewModel.OtherPolutants.Length.Should().BeGreaterThan(0);
            _stationViewModel.ProviderInfo.Should().NotBeNull();
            _stationViewModel.StationInfo.Should().NotBeNull();
            _stationViewModel.Timestamp.Should().Be(new DateTimeOffset(DateTime.Today).AddHours(23));
            _stationViewModel.TimestampCaption.Should().NotBeEmpty();
        }
    }

    public class when_building_stations_details_view_model : WebAppSpecs
    {
        private SmogDto[] _smogDtos;
        private StationDetailsViewModel _stationDetailsViewModel; 
        public when_building_stations_details_view_model()
        {
            _smogDtos = null;
            _stationDetailsViewModel = ViewModelFactory.CreateStationDetailsViewModel(_smogDtos,"todo",null,DateTimeOffset.Now);
        }

        [Fact(Skip = "To do")]
        public void it_should_create_details_view_model()
        {
            _stationDetailsViewModel.Should().NotBeNull();
        }
    }


}
