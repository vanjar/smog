﻿using System;
using Smog.Infrastructure.Model.Dictionaries;
using Smog.Infrastructure.Model.Enums;

namespace Smog.WebApp.Controllers.Station
{
    public class StationPolutantViewModel
    {
        public SmogPolutantType Type { get; set; }
        public string Name { get; set; }
        public SmogPolutantUnit Unit { get; set; }
        public string UnitName { get; set; }
        public SmogPolutantInterval Interval { get; set; }
        public string IntervalName { get; set; }
        public double Value { get; set; }
        public DateTimeOffset Timestamp { get; set; }

        public StationPolutantViewModel(SmogPolutantType type, SmogPolutantUnit unit,
            SmogPolutantInterval interval,  double value, DateTimeOffset timestamp)
        {
            Type = type;
            Name = SmogPolutantTypeDictionary.Get(type).ShortName;
            Unit = unit;
            UnitName = SmogPolutantUnitDictionary.Get(unit);
            Interval = interval;
            IntervalName = SmogPolutantIntervalDictionary.Get(interval);
            Value = value;
            Timestamp = timestamp;
        }
    }
}