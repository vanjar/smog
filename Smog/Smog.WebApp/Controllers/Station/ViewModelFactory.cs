﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeoCoordinatePortable;
using Humanizer;
using Smog.Infrastructure;
using Smog.Infrastructure.Cache;
using Smog.Infrastructure.Model;
using Smog.Infrastructure.Model.Enums;
using Smog.Infrastructure.Services.Aqi;

namespace Smog.WebApp.Controllers.Station
{
    public class ViewModelFactory
    {
        private readonly SmogCache _smogCache;
        private readonly IAqiCalculator _aqiCalculator;
        public ViewModelFactory(SmogCache smogCache, IEnumerable<IAqiCalculator> aqiCalculators)
        {
            _smogCache = smogCache;
            _aqiCalculator = aqiCalculators.Single(x => x.Id == AqiCalculatorTypes.UsEpa);
        }

        public StationInfoViewModel CreateStationInfoViewModel(SmogDto smogDto, GeoCoordinate clientGeoCoordinate)
        {
            if (smogDto == null)
                return null;

            var geoDto = _smogCache.GetStationGeoData(smogDto.Key);

            var distance = clientGeoCoordinate == null
                 ? (double?)null
                 : clientGeoCoordinate.GetDistanceTo(new GeoCoordinate(smogDto.Latitude, smogDto.Longitude));

            var stationInfo = new StationInfoViewModel(smogDto.StationId, smogDto.ProviderId, smogDto.StationName,
                geoDto?.Country, geoDto?.City, geoDto?.PostalNo, geoDto?.Street, geoDto?.StreetNo,
                smogDto.Longitude, smogDto.Latitude, distance);

            return stationInfo;
        }

        public StationDetailsViewModel CreateStationDetailsViewModel(SmogDto[] intervalSmogDtos, 
            string stationKey, GeoCoordinate clientGeoCoordinate, DateTimeOffset clientDateTime)
        {
            //get data from station i
            var cachedData = _smogCache.GetSmogData(stationKey);

            var latest = cachedData?.OrderByDescending(x => x.IntervalStart).FirstOrDefault();
            if (latest == null)
                return null;

            var stationInfo = CreateStationInfoViewModel(latest, clientGeoCoordinate);
            var providerInfo = new ProviderInfoViewModel(latest.ProviderId, latest.StationName);

            if (intervalSmogDtos == null || intervalSmogDtos.Length == 0)
              return new StationDetailsViewModel(null,new StationViewModel(_aqiCalculator.Id,null,null,null,stationInfo,providerInfo, latest.IntervalStart, 
                  latest.IntervalStart.Humanize(clientDateTime)));

            return null;
        }

        public StationViewModel CreateStationViewModel(SmogDto[] latestPast24Hours, GeoCoordinate clientGeoCoordinate,
            DateTimeOffset clientDateTime)
        {
            SmogDto latest = latestPast24Hours.OrderByDescending(x => x.IntervalStart).FirstOrDefault();
            if (latest == null)
                return null;

            var requiredNonHourIntervalsForAqi = new Tuple<SmogPolutantType, SmogPolutantInterval>[]
            {
                Tuple.Create(SmogPolutantType.O3, SmogPolutantInterval.EightHour),
                Tuple.Create(SmogPolutantType.Pm2Point5, SmogPolutantInterval.TwentyFourHour),
                Tuple.Create(SmogPolutantType.Pm10, SmogPolutantInterval.TwentyFourHour),
                Tuple.Create(SmogPolutantType.CO, SmogPolutantInterval.EightHour),
                Tuple.Create(SmogPolutantType.SO2, SmogPolutantInterval.TwentyFourHour),
            };

            List<Tuple<SmogPolutantDto, DateTimeOffset>> calculated = requiredNonHourIntervalsForAqi
                .Select(r => GetAverageFromLast(latestPast24Hours, latest, r.Item1, r.Item2))
                .Where(r => r != null)
                .ToList();

            latest.Polutants.Each(p =>
            {
                if (!calculated.Any(x => x.Item1.Type == p.Type && x.Item1.Interval == p.Interval))
                    calculated.Add(Tuple.Create(p, latest.IntervalStart));
            });

            var calculatedAqis = calculated.Select(p =>
            {
                var polutantInfo = new StationPolutantViewModel(p.Item1.Type,
                    p.Item1.Unit, p.Item1.Interval, p.Item1.Value, p.Item2);
                var aqi = _aqiCalculator.Calculate(p.Item1.Type, p.Item1.Interval, p.Item1.Unit, p.Item1.Value);
                if (aqi == null)
                    return new StationAqiPolutantViewModel(null, null, null, polutantInfo);
                var category = _aqiCalculator.GetCategoryByAqi(aqi.Value);
                return new StationAqiPolutantViewModel(category.Id, category.Name, aqi, polutantInfo);
            }).ToArray();
            var aqiPolutants = calculatedAqis.Where(aqi => aqi.Aqi != null).ToArray();
            var nonAqiPolutants = calculatedAqis.Where(aqi => aqi.Aqi == null).ToArray();
            var dominant = aqiPolutants.OrderBy(x => x.Aqi).LastOrDefault();

            var stationInfo = CreateStationInfoViewModel(latest, clientGeoCoordinate);

            var providerInfo = new ProviderInfoViewModel(latest.ProviderId, latest.StationName);
            var timestampCaption = latest.IntervalStart.Humanize(clientDateTime);
            return new StationViewModel(_aqiCalculator.Id, dominant,
                aqiPolutants.Where(x => x != dominant).ToArray(), nonAqiPolutants.Select(p => p.PolutantInfo).ToArray(), stationInfo, 
                providerInfo, latest.IntervalStart, timestampCaption);
        }


        private Tuple<SmogPolutantDto, DateTimeOffset> GetAverageFromLast(SmogDto[] latestPast24Hour, SmogDto latest, SmogPolutantType type,
            SmogPolutantInterval interval)
        {
            if (interval == SmogPolutantInterval.Hour)
                throw new ArgumentException("Hour interval not allowed for average.");
            var latestPolutant = latest.Polutants.SingleOrDefault(p => p.Interval == interval && p.Type == type);
            if (latestPolutant != null)
                return Tuple.Create(latestPolutant, latest.IntervalStart);

            //fallback to average
            var hours = interval == SmogPolutantInterval.EightHour ? 8 : 24;

            var validInputsForAverage = latestPast24Hour.Select(x =>
                    new
                    {
                        polutant = x.Polutants.SingleOrDefault(p => p.Type == type && p.Interval == SmogPolutantInterval.Hour),
                        timestamp = x.IntervalStart,
                    })
                .Where(x => x.polutant != null && x.timestamp >= latest.IntervalStart.AddHours(-hours))
                .ToArray();

            if (!validInputsForAverage.Any())
                return null;

            var avg = Math.Round(validInputsForAverage.Average(x => x.polutant.Value), 2);
            var timestamp = validInputsForAverage.Max(x => x.timestamp);
            return Tuple.Create(new SmogPolutantDto(avg, type, interval, validInputsForAverage.First().polutant.Unit), timestamp);
        }
    }
}
