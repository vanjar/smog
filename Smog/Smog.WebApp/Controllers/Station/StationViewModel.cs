﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smog.WebApp.Controllers.Station
{
    public class StationViewModel
    {
        public string AqiCalculatorType { get; set; }

        public StationAqiPolutantViewModel DominantAqiPolutant { get; set; }

        public StationAqiPolutantViewModel[] AqiPolutants { get; set; }

        public StationPolutantViewModel[] OtherPolutants { get; set; }
 
        public StationInfoViewModel StationInfo { get; set; }

        public ProviderInfoViewModel ProviderInfo { get; set; }

        public DateTimeOffset Timestamp { get; set; }

        public string TimestampCaption { get; set; }
        public StationViewModel(string aqiCalculatorType, StationAqiPolutantViewModel dominantAqiPolutant, StationAqiPolutantViewModel[] aqiPolutants,
            StationPolutantViewModel[] otherPolutants, StationInfoViewModel stationInfo, ProviderInfoViewModel providerInfo, DateTimeOffset timestamp, 
            string timestampCaption)
        {
            AqiCalculatorType = aqiCalculatorType;
            DominantAqiPolutant = dominantAqiPolutant;
            AqiPolutants = aqiPolutants;
            OtherPolutants = otherPolutants;
            StationInfo = stationInfo;
            ProviderInfo = providerInfo;
            Timestamp = timestamp;
            TimestampCaption = timestampCaption;
        }
 
    }
}
