using Smog.Infrastructure.Model;

namespace Smog.WebApp.Controllers.Station
{
    public class StationInfoViewModel
    {
        public string StationId { get; set; }
        public string ProviderId { get; set; }
        public string StationName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string PostalNo { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string Address => $"{Street} {StreetNo}, {PostalNo} {City}, {Country}";
        public double Lon { get; set; }
        public double Lat { get; set; }
        public string Key => StationKey.Create(StationId, ProviderId);
        public double? Distance { get; set; }
        public string DistanceCaption { get; set; }
        public StationInfoViewModel(string stationId,string providerId, string stationName, string country, string city, string postalNo, string street,
            string streetNo, double lon, double lat, double? distance)
        {
            StationId = stationId;
            ProviderId = providerId;
            StationName = stationName;
            Country = country;
            City = city;
            PostalNo = postalNo;
            Street = street;
            StreetNo = streetNo;
            Lon = lon;
            Lat = lat;
            Distance = distance;
            DistanceCaption = GetReadableDistance(distance);
        }

        private string GetReadableDistance(double? distance)
        {
            if (distance == null)
                return null;

            if (distance < 1000)
                return $"{(int)distance} m";

            return $"{(int)distance / 1000} km";
        }
    }
}