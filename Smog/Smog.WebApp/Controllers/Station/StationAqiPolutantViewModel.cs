namespace Smog.WebApp.Controllers.Station
{
    public class StationAqiPolutantViewModel
    {
        public int? Category { get; set; }
        public string CategoryName { get; set; }
        public int? Aqi { get; set; }

        public StationPolutantViewModel PolutantInfo { get; set; }
        public StationAqiPolutantViewModel(int? category, string categoryName, int? aqi, StationPolutantViewModel polutantInfo)
        {
            PolutantInfo = polutantInfo;
            Category = category;
            CategoryName = categoryName;
            Aqi = aqi;
        }
    }
}