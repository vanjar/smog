﻿using System;
using Smog.Infrastructure.Model;
using Smog.Infrastructure.Model.Enums;

namespace Smog.WebApp.Controllers.Station
{
    public class StationDetailsPolutantAqiValueViewModel
    {
        public int? Category { get; private set; }
        public string CategoryName { get; private set; }
        public int? Aqi { get; private set; }

        public StationDetailsPolutantAqiValueViewModel(int? category, string categoryName, int? aqi)
        {
            Category = category;
            CategoryName = categoryName;
            Aqi = aqi;
        }
    }

    public class StationDetailsPolutantValueViewModel
    {
        public DateTimeOffset Timestamp { get; private set; }
        public double Value { get; private set; }
        public StationDetailsPolutantAqiValueViewModel Aqi { get; private set; }

        public StationDetailsPolutantValueViewModel(DateTimeOffset timestamp, double value, 
            StationDetailsPolutantAqiValueViewModel aqi)
        {
            Timestamp = timestamp;
            Value = value;
            Aqi = aqi;
        }
    }

    public class StationDetailsPolutantViewModel
    {
        public SmogPolutantType Type { get; private set; }
        public string Name { get; private set; }
        public SmogPolutantUnit Unit { get; private set; }
        public string UnitName { get; private set; }
        public SmogPolutantInterval Interval { get; private set; }
        public string IntervalName { get; private set; }
        public  StationDetailsPolutantValueViewModel[] PolutantValues { get; private set; }

        public StationDetailsPolutantViewModel(SmogPolutantType type, string name, SmogPolutantUnit unit, 
            string unitName, SmogPolutantInterval interval, string intervalName, StationDetailsPolutantValueViewModel[] polutantValues)
        {
            Type = type;
            Name = name;
            Unit = unit;
            UnitName = unitName;
            Interval = interval;
            IntervalName = intervalName;
            PolutantValues = polutantValues;
        }
    }

    public class StationDetailsViewModel
    {
        public StationDetailsPolutantViewModel[] PolutantsData { get; private set; }
        public StationViewModel Station { get; private set; }

        public StationDetailsViewModel(StationDetailsPolutantViewModel[] polutantsData, StationViewModel station)
        {
            PolutantsData = polutantsData;
            Station = station;
        }
 
    }
}
