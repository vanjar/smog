﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Smog.Infrastructure;
using Smog.Infrastructure.Cache;
using Smog.Infrastructure.DataAccess;
using Smog.Infrastructure.Model;
using GeoCoordinatePortable;
using Smog.Infrastructure.Services;

namespace Smog.WebApp.Controllers.Station
{
    public class TmpDateResult
    {
        public DateTimeOffset DateTimeOffset { get; set; }
        public DateTimeOffset UtcTime { get; set; }
        public string JsDate { get; set; }
    }


    [Route("station")]
    public class StationController : Controller
    {
        private readonly SmogCache _smogCache;
        private readonly ISmogDatabase _database;
        private readonly ViewModelFactory _viewModelFactory;
        public StationController(SmogCache smogCache, ISmogDatabase database,ViewModelFactory viewModelFactory)
        {
            _smogCache = smogCache;
            _database = database;
            _viewModelFactory = viewModelFactory;
        }

        [HttpGet]
        [Route("date")]
        public async Task<TmpDateResult> DateAsync()
        {
            var date = new DateTimeOffset(new DateTime(2016,12,30,10,22,00), TimeSpan.FromHours(-3));
            return new TmpDateResult
            {
                DateTimeOffset = date,
                UtcTime = date.ToUniversalTime(),
            };
        }
        
        [HttpGet]
        [Route("{stationKey}")]
        public async Task<StationViewModel> GetAsync(string stationKey, double? lat, double? lng, DateTimeOffset localDateTime)
        {
          var cachedData = _smogCache.GetSmogData(stationKey);
            if (cachedData == null)
                return null;
            return _viewModelFactory.CreateStationViewModel(cachedData.ToArray(),  GetGeoCoordinate(lat,lng), localDateTime);
        }

        [HttpGet]
        [Route("search")]
        public async Task<string[]> SearchAsync(string s, double? lat, double? lng)
        {
            IEnumerable<StationGeoDto> geoData = _smogCache.GetStationGeoData();
            if (!string.IsNullOrEmpty(s))
                geoData =geoData.Where(x =>CultureInfo.InvariantCulture.CompareInfo.IndexOf(x.FormattedAddress, s,CompareOptions.IgnoreCase) >= 0);

            var geoCoordinate = GetGeoCoordinate(lat, lng);

            if (geoCoordinate == null)
                return geoData.Select(x => x.Key).ToArray();

            var ordered = geoData
                .OrderBy(g => geoCoordinate.GetDistanceTo(new GeoCoordinate(g.Latitude, g.Longitude)));
            
            return ordered
                .Select(x => x.Key)
                .ToArray();
        }
        
        [HttpGet]
        public async Task<IEnumerable<StationViewModel>> GetAsync(string[] key,
            double? lat, double? lng, int cols, DateTimeOffset localDateTime)
        {
            if (key == null || key.Length == 0)
                return Enumerable.Empty<StationViewModel>();
            
            var smogData = _smogCache.GetSmogData(key);
            var stations = new List<StationViewModel>();
            
            smogData.Each(x => stations.Add(_viewModelFactory.CreateStationViewModel(smogData[x.Key].ToArray(),GetGeoCoordinate(lat,lng), localDateTime)));

            var result = stations
                .OrderBy(x => x.StationInfo.Distance)
                .ToArray();

            if (cols > 1)
                result = result.OrderByMasonry(cols).ToArray();

            return result.ToArray();
        }

        [Route("mapsearch")]
        [HttpGet]
        public async Task<StationViewModel[]> GetMapAsync(double? latFrom, double? latTo, double? lngFrom, double? lngTo,
             double? lat, double? lng, DateTimeOffset clientDateTime)
        {
            if (latFrom == null || latTo == null || lngFrom == null || lngTo == null)
                return null;

            IEnumerable<StationGeoDto> geoData = _smogCache.GetStationGeoData();
            var geoCoordinate = GetGeoCoordinate(lat, lng);
            Dictionary<string, List<SmogDto>> stations = _smogCache.GetSmogData(geoData.Select(x => x.Key).ToArray());

            return geoData.Where(g =>
                g.Latitude >= latFrom.Value && g.Latitude <= latTo.Value && g.Longitude >= lngFrom.Value &&
                g.Longitude <= lngTo.Value).
                Select(g=> _viewModelFactory.CreateStationViewModel(stations[g.Key].ToArray(), geoCoordinate, clientDateTime))
                .ToArray();
        }

        [Route("nearest")]
        [HttpGet]
        public async Task<StationViewModel> GetNearestAsync(double? lat, double? lng, DateTimeOffset clientDateTime)
        {
            if (lat == null || lng == null)
                return null;

            IEnumerable<StationGeoDto> geoData = _smogCache.GetStationGeoData();
            
            var geoCoordinate = GetGeoCoordinate(lat, lng);
            
            var nearest = geoData
                .OrderBy(g => geoCoordinate.GetDistanceTo(new GeoCoordinate(g.Latitude, g.Longitude)))
                .FirstOrDefault();

            if (nearest == null)
                return null;

            return _viewModelFactory.CreateStationViewModel(_smogCache.GetSmogData(nearest.Key).ToArray(), geoCoordinate,
                clientDateTime);
        }

        [HttpGet]
        [Route("details/{stationKey}")]
        public async Task<StationDetailsViewModel> DetailsAsync(string stationKey, double? lat, double? lng, DateTimeOffset localDateTime)
        {
            var cachedGeo = _smogCache.GetStationGeoData(stationKey);
            var cachedData = _smogCache.GetSmogData(stationKey);
            if (cachedData == null)
                return null;
            //get smog data
            var detailsData = await _database.GetSmogDataDetailsAsync(stationKey, DateTimeOffset.Now);
            var polutantTypes = detailsData.SelectMany(x => x.Polutants)
                .Select(x=> new {x.Interval, x.Type, x.Unit}).Distinct();
            return null;
            //return CreateFromLatestPast24HourDtos(cachedData.ToArray(), cachedGeo, GetGeoCoordinate(lat, lng), localDateTime);
        }

        private GeoCoordinate GetGeoCoordinate(double? lat, double? lng)
        {
            return lat == null ? null : lng == null ? null : new GeoCoordinate(lat.Value, lng.Value);
        }


    }
}

