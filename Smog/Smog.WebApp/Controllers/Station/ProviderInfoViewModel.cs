﻿namespace Smog.WebApp.Controllers.Station
{
    public class ProviderInfoViewModel
    {
        public string ProviderName { get; set; }
        public string ProviderInfo { get; set; }

        public ProviderInfoViewModel(string providerName, string providerInfo)
        {
            ProviderName = providerName;
            ProviderInfo = providerInfo;
        }
    }
}