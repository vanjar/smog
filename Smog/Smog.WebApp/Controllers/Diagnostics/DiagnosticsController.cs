﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Smog.Infrastructure.DataAccess;

namespace Smog.WebApp.Controllers.Diagnostics
{
    [Route("diagnostics")]
    public class DiagnosticsController : Controller
    {
        private readonly SmogDatabase _database;

        public DiagnosticsController(SmogDatabase database)
        {
            _database = database;
        }

        [HttpGet]
        [Route("latest")]
        public async Task<IEnumerable<DiagnosticsViewModel>> GetLatestAsync()
        {
            Log.ForContext<DiagnosticsController>().Information("GetLatestAsync started.");
            var dtos = await _database.GetLatestSmogDataByStationAsync().ConfigureAwait(false);
            var result = dtos.Select(DiagnosticsViewModel.CreateFromDto);
            Log.ForContext<DiagnosticsController>().Information("GetLatestAsync finished.");
            return result;
        }

        [HttpGet]
        [Route("providers")]
        public async Task<IEnumerable<DiagnosticsDataProviderInfoViewModel>> GetProviderInfosAsync()
        {
            Log.ForContext<DiagnosticsController>().Information("GetProviderInfosAsync started.");
            var infos = await _database.GetDataProviderInfosAsync().ConfigureAwait(false);
            var result =  infos.Select(DiagnosticsDataProviderInfoViewModel.CreateFromDto).OrderByDescending(x=> x.Start);
            Log.ForContext<DiagnosticsController>().Information("GetProviderInfosAsync finished.");
            return result;
        }
    }
}
