using System;
using Smog.Infrastructure.Model;

namespace Smog.WebApp.Controllers.Diagnostics
{
    public class DiagnosticsDataProviderInfoViewModel
    {
        public string DataProviderInfoId { get; set; }
        public string DataProviderId { get; set; }
        public DateTimeOffset Start { get; set; }
        public DateTimeOffset? Stop { get; set; }
        public string StatusDescription { get; set; }
        public int Status { get; set; }
        public string Info { get; set; }

        public static DiagnosticsDataProviderInfoViewModel CreateFromDto(DataProviderInfoDto dto)
        {
            return new DiagnosticsDataProviderInfoViewModel
            {
                DataProviderInfoId = dto.DataProviderInfoId,
                DataProviderId = dto.ProviderId,
                Status = (int) dto.Status,
                StatusDescription = dto.Status.ToString("g"),
                Start = dto.LastFetchStart,
                Stop = dto.LastFetchStop,
                Info = dto.Info
            };
        }
    }
}