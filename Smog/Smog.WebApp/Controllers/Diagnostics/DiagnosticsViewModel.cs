using System;
using System.Linq;
using Smog.Infrastructure.Model;
using Smog.Infrastructure.Model.Dictionaries;

namespace Smog.WebApp.Controllers.Diagnostics
{
    //TODO: to naj bo kar tapravi model za podatke postaje
    public class DiagnosticsViewModel
    {
        public string StationId { get; set; }
        public string Name { get; set; }
        public DateTimeOffset Date { get; set; }
        public string[] Polutants { get; set; }

        public static DiagnosticsViewModel CreateFromDto(SmogDto dto)
        {
            return new DiagnosticsViewModel
            {
                Polutants = dto.Polutants.OrderBy(x => x.Type)
                    .Select(p =>$"{SmogPolutantTypeDictionary.Get(p.Type).ShortName} " +
                                $"({SmogPolutantIntervalDictionary.Get(p.Interval)}):" +
                                $" {p.Value} {SmogPolutantUnitDictionary.Get(p.Unit)}")
                    .ToArray(),
                Date = dto.IntervalStart,
                Name = dto.StationName,
                StationId = dto.StationId
            };

        }
    }
}