﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Smog.Infrastructure.Model.Enums;

namespace Smog.WebApp.Controllers.Aqi
{
    public class PolutantAqiViewModel
    {
        public int PolutantType { get; set; }
        public string PolutantName { get; set; }
        public string PolutantUnit { get; set; }
        public AqiCategoryViewModel AqiCategory { get; set; }
    }

    public class AqiCategoryViewModel
    {
        public int CategoryType { get; set; }

        public string CategoryName { get; set; }
    }

    public class AqiViewModel
    {
        public int Aqi { get; set; }
        public AqiCategoryViewModel AqiCategory { get; set; }
        public List<PolutantAqiViewModel> PolutantAqis { get; set; }
    }
}
