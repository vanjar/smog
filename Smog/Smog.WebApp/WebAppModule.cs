﻿using Autofac;
using Smog.WebApp.Controllers.Station;

namespace Smog.WebApp
{
    public class WebAppModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ViewModelFactory>().SingleInstance();
          
        }
    }
}
