﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Options;
using Serilog;
using Serilog.Events;
using Smog.Infrastructure;
using Smog.Infrastructure.Configuration;
using Smog.Infrastructure.DataAccess;
using Smog.Infrastructure.DataProviders;
using Smog.Infrastructure.Services;
using Smog.Infrastructure.Logging;
using Smog.WebApp.Controllers.Station;

namespace Smog.WebApp
{
    public class Startup
    {

        public IContainer ApplicationContainer { get; private set; }    

        public IConfigurationRoot Configuration { get; private set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            this.Configuration = builder.Build();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<DataProvidersOptions>(Configuration.GetSection("dataProviders"));
            services.AddMvc();
            services.AddCors();
            ApplicationContainer = CreateContainer(services);
            return new AutofacServiceProvider(ApplicationContainer);
        }

       
        private IContainer CreateContainer(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<InfrastructureModule>();
            builder.RegisterModule<DataAccessModule>();
            builder.RegisterModule<WebAppModule>();
            builder.Populate(services);
            return builder.Build();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
              IApplicationLifetime appLifetime, IEnumerable<ISmogDataService> smogDataServices,
              IOptions<DataProvidersOptions> dataProviderOptions,IEnumerable<IService> services)
        {
            loggerFactory.AddSerilog();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(builder =>
              builder.WithOrigins("http://localhost:3000")
                     .AllowAnyHeader()
                     .AllowAnyMethod()
                    .Build()
            );
            app.UseMvc();
          
            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Smog Web Host started.");
            });

            appLifetime.ApplicationStopped.Register(() => ApplicationContainer.Dispose());
            appLifetime.ApplicationStarted.Register(() =>
            {
                StartSmogDataServices(smogDataServices, dataProviderOptions);
                StartServices(services);
             //   stationGeoDataService.StartAsync(CancellationToken.None).Wait();
            });
         ;
        }


        private void StartServices(IEnumerable<IService> services)
        {
           services.Each(s=> s.StartAsync(CancellationToken.None).Wait());
        }

        private void StartSmogDataServices(IEnumerable<ISmogDataService> smogDataServices,
            IOptions<DataProvidersOptions> options)
        {
            if (options.Value.Disabled)
                return;

            smogDataServices.Where(s =>options.Value.Disable == null ||options.Value.Disable.All(id => id != s.SmogDataProvider.ProviderId))
                .Cast<IService>().Each(service => service.StartAsync(CancellationToken.None).Wait());
        }
    }
}
