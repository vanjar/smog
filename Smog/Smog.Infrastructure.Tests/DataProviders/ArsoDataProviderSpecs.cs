﻿using System.IO;
using System.Xml.Serialization;
using Autofac;
using FluentAssertions;
using Smog.Infrastructure.DataProviders;
using Smog.Infrastructure.DataProviders.Arso;
using Xunit;

namespace Smog.Infrastructure.Tests.DataProviders
{
    public class ArsoDataProviderSpecs : SmogInfrastructureSpec
    {
        private static ISmogDataProvider _arsoDataProvider;

        public ArsoDataProviderSpecs()
        {
            _arsoDataProvider = Container.Resolve<ArsoDataProvider>();
        }

        public class when_getting_data : ArsoDataProviderSpecs
        {
            [Fact]
            public void It_should_return_smog_values()
            {
                var data = _arsoDataProvider.GetDataAsync().Result;
                data.Should().NotBeNull();
            }
        }
    }

  
}
