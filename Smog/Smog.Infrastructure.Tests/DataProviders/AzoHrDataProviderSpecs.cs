﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using Smog.Infrastructure.DataProviders;
using Smog.Infrastructure.DataProviders.Azohr;
using Xunit;
using FluentAssertions;

namespace Smog.Infrastructure.Tests.DataProviders
{
    public class AzoHrDataProviderSpecs : SmogInfrastructureSpec
    {
        private static ISmogDataProvider _azoHrDataProvider;
        private readonly AzoHrMetaData _azoHrMetaData;

        public AzoHrDataProviderSpecs()
        {
            _azoHrMetaData = Container.Resolve<AzoHrMetaData>();
            _azoHrDataProvider = Container.Resolve<AzoHrDataProvider>();
         }

        public class when_getting_azo_hr_meta_data : AzoHrDataProviderSpecs
        {
            private readonly Dictionary<int, AzoHrStation> _stationsMetaData;
            private readonly Dictionary<int, string> _polutantMetaData;
            private readonly Dictionary<int, string> _dataTypeMetaData;
            private readonly Dictionary<int, string> _secondaryPolutantMetaData;
            private readonly  int[]_actualPlutantData;
            public when_getting_azo_hr_meta_data()
            {
               _stationsMetaData = _azoHrMetaData.GetStations();
                _polutantMetaData = _azoHrMetaData.GetPolutantTypes();
                _dataTypeMetaData = _azoHrMetaData.GetDataTypes();
                _secondaryPolutantMetaData = _azoHrMetaData.GetSecondaryPolutantTypes();
                _actualPlutantData = _azoHrMetaData.GetHourlyPolutantTypes();
            }
            [Fact]
            public void It_should_return_stations_meta_data()
            {

                _stationsMetaData.Should().NotBeNull();
                _stationsMetaData.Should().HaveCount(60);
            }

            [Fact]
            public void It_should_return_polutant_meta_data()
            {

                _polutantMetaData.Should().NotBeNull();
                _polutantMetaData.Should().HaveCount(16);
            }

            [Fact]
            public void It_should_return_data_types_meta_data()
            {

                _dataTypeMetaData.Should().NotBeNull();
                _dataTypeMetaData.Should().HaveCount(3);
            }

            [Fact]
            public void It_should_return_secondary_polutant_meta_data()
            {

                _secondaryPolutantMetaData.Should().NotBeNull();
                _secondaryPolutantMetaData.Should().HaveCount(35);
            }

            public void It_should_return_actual_polutant_meta_data()
            {

                _actualPlutantData.Should().NotBeNull();
                _actualPlutantData.Should().HaveCount(10);
            }
        }

        public class when_running_azo_hr_meta_data_diagnostics : AzoHrDataProviderSpecs
        {
            private string _diagnosticsFile = "";
            public when_running_azo_hr_meta_data_diagnostics()
            {
                _diagnosticsFile = _azoHrMetaData.RunQueryDiagnostics("c:\\temp\\azohr-diagnostics.txt", 300);
            }

            [Fact(Skip="Diagnostics should be run only if something changes in AzoHr api")]
            public void It_should_store_azo_hr_meta_data_statistics_statistics()
            {
                _diagnosticsFile.Should().NotBeNullOrEmpty();
            }
        }

        public class when_getting_data : AzoHrDataProviderSpecs
        {
            public when_getting_data()
            {
                ((ITestableSmogDataProvider) _azoHrDataProvider).NumberOfAllowedRequests = 5;
                ((ITestableSmogDataProvider) _azoHrDataProvider).DelayInMilisecondsBetweenRequests = 100;

            }

            [Fact]
            public void It_should_return_smog_values()
            {
                var data = _azoHrDataProvider.GetDataAsync().Result;
                data.Count().Should().BeGreaterThan(1);
            }
        }
    }
}
