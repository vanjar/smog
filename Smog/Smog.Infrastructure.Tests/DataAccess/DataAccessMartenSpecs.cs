﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Smog.Infrastructure.DataProviders.Arso;
using Smog.Infrastructure.Model;
using Smog.Infrastructure.Model.Enums;
using Xunit;
// ReSharper disable InconsistentNaming

namespace Smog.Infrastructure.Tests.DataAccessMarten
{

    public class when_getting_smog_data_by_data_id : SmogInfrastructureSpec
    {
        private static SmogDto _smogDto;

        public when_getting_smog_data_by_data_id()
        {
            _smogDto = new SmogDto(DateTime.Now, DateTime.Now, "stationName", "stationId",
                1.01, 2.02, 333.33, "SLO", "providerId", "responseHashe", DateTime.Now.AddHours(-1), DateTime.Now,
                TimeZoneInfo.Local.Id,
                new SmogPolutantDto(2.2, SmogPolutantType.SO2, SmogPolutantInterval.Hour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre),
                new SmogPolutantDto(3.3, SmogPolutantType.NH3, SmogPolutantInterval.TwentyFourHour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre),
                new SmogPolutantDto(3.3, SmogPolutantType.Benzene, SmogPolutantInterval.EightHour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre));
            Database.InsertSmogDataAsync(_smogDto).Wait();
        }

        [Fact]
        public void It_should_return_smog_data()
        {
            var savedSmogDto = Database.GetSmogDataByIdAsync(_smogDto.SmogDataId).Result;
            savedSmogDto.ShouldBeEquivalentTo(_smogDto);

        }
    }

    public class when_getting_smog_data_by_provider_id : SmogInfrastructureSpec
    {
        private static SmogDto _smogDto;

        public when_getting_smog_data_by_provider_id()
        {
            _smogDto = new SmogDto(DateTime.Now, DateTime.Now.AddHours(-1), "stationName", "stationId",
                1.01, 2.02, 333.33, "SLO", "arsoId", "responseHashe", DateTime.Now.AddHours(-3),
                DateTime.Now.AddHours(-2), TimeZoneInfo.Local.Id,
                new SmogPolutantDto(2.2, SmogPolutantType.SO2, SmogPolutantInterval.Hour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre),
                new SmogPolutantDto(3.3, SmogPolutantType.NH3, SmogPolutantInterval.TwentyFourHour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre),
                new SmogPolutantDto(3.3, SmogPolutantType.Benzene, SmogPolutantInterval.EightHour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre));
            Database.InsertSmogDataAsync(_smogDto).Wait();
        }

        [Fact]
        public void It_should_return_smog_data_of_provider()
        {
            var providerSmogDataDto = Database.GetSmogDataByProviderIdAsync("arsoId").Result.ToArray();
            providerSmogDataDto.Single().ShouldBeEquivalentTo(_smogDto);
        }
    }


    public class when_getting_smog_data_by_interval : SmogInfrastructureSpec
    {

        public when_getting_smog_data_by_interval()
        {

            Database.InsertSmogDataAsync(new SmogDto(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1).AddHours(1),
                "stationName", "id1",
                1.01, 2.02, 333.33, "SLO", "arsoId", "responseHashe", DateTime.Now.AddHours(-3),
                DateTime.Now.AddHours(-2), TimeZoneInfo.Local.Id,
                new SmogPolutantDto(2.2, SmogPolutantType.SO2, SmogPolutantInterval.Hour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre),
                new SmogPolutantDto(3.3, SmogPolutantType.NH3, SmogPolutantInterval.TwentyFourHour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre),
                new SmogPolutantDto(3.3, SmogPolutantType.Benzene, SmogPolutantInterval.EightHour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre))).Wait();
            Database.InsertSmogDataAsync(new SmogDto(DateTime.Now.AddDays(1), DateTime.Now.AddDays(1).AddHours(1),
                "stationName", "id2",
                1.01, 2.02, 333.33, "SLO", "arsoId", "responseHashe", DateTime.Now.AddHours(-3),
                DateTime.Now.AddHours(-2), TimeZoneInfo.Local.Id,
                new SmogPolutantDto(2.2, SmogPolutantType.SO2, SmogPolutantInterval.Hour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre),
                new SmogPolutantDto(3.3, SmogPolutantType.NH3, SmogPolutantInterval.TwentyFourHour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre))).Wait();
            Database.InsertSmogDataAsync(new SmogDto(DateTime.Now.AddDays(2), DateTime.Now.AddDays(2).AddHours(1),
                "stationName", "id3",
                1.01, 2.02, 333.33, "SLO", "arsoId", "responseHashe", DateTime.Now.AddHours(-3),
                DateTime.Now.AddHours(-2), TimeZoneInfo.Local.Id,
                new SmogPolutantDto(2.2, SmogPolutantType.SO2, SmogPolutantInterval.Hour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre),
                new SmogPolutantDto(3.3, SmogPolutantType.Benzene, SmogPolutantInterval.EightHour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre))).Wait();

        }

        [Fact]
        public void It_should_return_smog_data_in_interval()
        {
            var providerSmogDataDto =
                Database.GetSmogDataByIntervalAsync(DateTime.Now.Date.AddDays(-1), DateTime.Now.Date.AddDays(1))
                    .Result.ToArray();
            providerSmogDataDto.Select(x => x.StationId).Should().Contain(new[] {"id1", "id2"});
        }
    }

    public class when_inserting_and_data_already_exists : SmogInfrastructureSpec
    {

        private static SmogDto _smogDto;

        public when_inserting_and_data_already_exists()
        {
            var dateTime = DateTime.Now;
            Database.InsertSmogDataAsync(new SmogDto(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1).AddHours(1),
                "stationName", "id1",
                1.01, 2.02, 333.33, "SLO", "arsoId", "responseHashe",
                DateTime.Now.AddHours(-3), DateTime.Now.AddHours(-2), TimeZoneInfo.Local.Id,
                new SmogPolutantDto(2.2, SmogPolutantType.SO2, SmogPolutantInterval.Hour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre))).Wait();

            //owerwrite
            _smogDto = new SmogDto(DateTime.Now.AddDays(-2), DateTime.Now.AddDays(-2).AddHours(1), "stationName2",
                "id1", 2.01, 3.02, 44.44, "SLO2", "arsoId", "responseHashe",
                DateTime.Now.AddHours(-3), DateTime.Now.AddHours(-2), TimeZoneInfo.Local.Id,
                new SmogPolutantDto(2.2, SmogPolutantType.SO2, SmogPolutantInterval.Hour,
                    SmogPolutantUnit.MikroGramsPerCubicMetre));
            Database.InsertSmogDataAsync(_smogDto).Wait();
        }

        [Fact]
        public void It_should_overwrite_existing_data()
        {
            var providerSmogDataDto =
                Database.GetSmogDataByIntervalAsync(DateTime.Now.Date.AddDays(-1).ToUniversalTime(),
                    DateTime.Now.Date.AddDays(1).ToUniversalTime()).Result.ToArray();
            providerSmogDataDto.Select(x => x.StationId).Single().ToJson().Should().Equals(_smogDto.ToJson());
        }
    }

    public class when_querying_data_from_different_time_zones : SmogInfrastructureSpec
    {
        public when_querying_data_from_different_time_zones()
        {
            var newyorkDto = CreateSmogDto("NewYork", "pId",

                TimeZoneInfo.ConvertTime(DateTimeOffset.Now,
                    TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")));

            var laDto = CreateSmogDto("Los Angeles", "pId",
                TimeZoneInfo.ConvertTime(DateTimeOffset.Now,
                    TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time")));

            var londonDto = CreateSmogDto("London", "pId",
                TimeZoneInfo.ConvertTime(DateTimeOffset.Now,
                    TimeZoneInfo.FindSystemTimeZoneById("Greenwich Standard Time")));

            var moscowDto = CreateSmogDto("Moscow", "pId",
                TimeZoneInfo.ConvertTime(DateTimeOffset.Now,
                    TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time")));

            var tokioDto = CreateSmogDto("Tokio", "pId",
                TimeZoneInfo.ConvertTime(DateTimeOffset.Now, TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time")));


            Database.InsertSmogDataAsync(newyorkDto).Wait();
            Database.InsertSmogDataAsync(laDto).Wait();
            Database.InsertSmogDataAsync(londonDto).Wait();
            Database.InsertSmogDataAsync(moscowDto).Wait();
            Database.InsertSmogDataAsync(tokioDto).Wait();
        }

        [Fact]
        public void It_should_return_data_based_on_utc_query_time()
        {
            var data = Database.GetSmogDataByIntervalAsync(DateTimeOffset.Now, DateTimeOffset.Now.AddHours(1)).Result;
            data.Count().Should().Be(5);
        }
    }

    public class when_querying_latest_data_for_all_stations : SmogInfrastructureSpec
    {
        private static SmogDto _stationOneDto;
        private static SmogDto _stationTwoDto;
        public when_querying_latest_data_for_all_stations()
        {
            _stationOneDto = CreateSmogDto("1", "pId", DateTimeOffset.Now);
            _stationTwoDto = CreateSmogDto("2", "pId", DateTimeOffset.Now.AddDays(-1));

            Database.InsertSmogDataAsync(CreateSmogDto("1", "pId", DateTimeOffset.Now.AddDays(-1))).Wait();
            Database.InsertSmogDataAsync(_stationOneDto).Wait();
            Database.InsertSmogDataAsync(CreateSmogDto("2", "pId", DateTimeOffset.Now.AddDays(-2))).Wait();
            Database.InsertSmogDataAsync(_stationTwoDto).Wait();
        }

        [Fact]
        public void It_should_return_distinct_stations_with_latest_measurement_value_order_by_date()
        {
            var data = Database.GetLatestSmogDataByStationAsync().Result;
            data.ToArray().Length.Should().Be(2);
            data.Select(x=> x.SmogDataId).Should().ContainInOrder(_stationOneDto.SmogDataId, _stationTwoDto.SmogDataId);
        }
    }

    public class when_querying_latest_data_for_100_stations : SmogInfrastructureSpec
    {
        public when_querying_latest_data_for_100_stations()
        {
            for (int i = 0; i < 100; i++)
            {
                Database.InsertSmogDataAsync(CreateSmogDto($"Station{i}", "pId", DateTimeOffset.Now)).Wait();
            }
        }

        [Fact]
        public void It_should_return_distinct_stations_with_latest_measuremnet_value_order_by_date()
        {
            var data = Database.GetLatestSmogDataByStationAsync().Result;
            data.Count().Should().Be(100);
        }
    }


    public class when_querying_latest_past24_hours_data_for_specific_station : SmogInfrastructureSpec
    {
        private static string  _stationId = "stationId";
        private static string _providerId = "providerId";
        public when_querying_latest_past24_hours_data_for_specific_station()
        {
            Database.InsertSmogDataAsync(CreateSmogDto(_stationId,_providerId, DateTimeOffset.Now)).Wait();
            Database.InsertSmogDataAsync(CreateSmogDto(_stationId, _providerId, DateTimeOffset.Now.AddHours(-1))).Wait();
            Database.InsertSmogDataAsync(CreateSmogDto(_stationId, _providerId, DateTimeOffset.Now.AddHours(-25))).Wait();
            Database.InsertSmogDataAsync(CreateSmogDto(_stationId, _providerId, DateTimeOffset.Now.AddHours(-24))).Wait();
            Database.InsertSmogDataAsync(CreateSmogDto(_stationId, _providerId, DateTimeOffset.Now.AddHours(1))).Wait();
            Database.InsertSmogDataAsync(CreateSmogDto(_stationId, _providerId, DateTimeOffset.Now.AddHours(-2))).Wait();
        }

        [Fact]
        public void It_should_return_latest_past_24_hour_station_data()
        {
            var data = Database.GetLatestPast24HourSmogDataAsync(StationKey.Create(_stationId,_providerId)).Result;
            
            data[StationKey.Create(_stationId,_providerId)].Count.Should().Be(4);
        }
    }

    public class when_querying_latest_past24_hours_data_for_all_stations : SmogInfrastructureSpec
    {
        private static string _stationId1 = "stationId1";
        private static string _stationId2 = "stationId2";

        public when_querying_latest_past24_hours_data_for_all_stations()
        {
            Database.InsertSmogDataAsync(CreateSmogDto(_stationId1, "providerId", DateTimeOffset.Now)).Wait();
            Database.InsertSmogDataAsync(CreateSmogDto(_stationId2, "providerId", DateTimeOffset.Now.AddHours(-1))).Wait();
            
        }

        [Fact]
        public void It_should_return_latest_past_24_hour_station_data()
        {
            var data = Database.GetLatestPast24HourSmogDataAsync().Result;
            data.Keys.Count().Should().Be(2);
        }
    }

    public class when_querying_latest_data_for_single_station : SmogInfrastructureSpec
    {
        private static SmogDto _stationOneDto;
        public when_querying_latest_data_for_single_station()
        {
            _stationOneDto = CreateSmogDto("1","pId", DateTimeOffset.Now);
            var stationTwoDto = CreateSmogDto("2","pId", DateTimeOffset.Now.AddDays(-1));

            Database.InsertSmogDataAsync(CreateSmogDto("1", "pId", DateTimeOffset.Now.AddDays(-1))).Wait();
            Database.InsertSmogDataAsync(_stationOneDto).Wait();
            Database.InsertSmogDataAsync(CreateSmogDto("2", "pId", DateTimeOffset.Now.AddDays(-2))).Wait();
            Database.InsertSmogDataAsync(stationTwoDto).Wait();
        }

        [Fact]
        public void It_should_return_station()
        {
            var data = Database.GetLatestSmogDataForStationAsync("1").Result;
            data.SmogDataId.Should().Be(_stationOneDto.SmogDataId);
        }
    }

    public class when_saving_data_provider_info_with_fetch_end_offset : SmogInfrastructureSpec
    {

        private static DataProviderInfoDto _dto;
        private static DataProviderInfoDto[] _dtos;
        public when_saving_data_provider_info_with_fetch_end_offset()
        {
            _dto = DataProviderInfoDto.CreateNew("provider", DateTimeOffset.Now);
            Database.SaveDataProviderInfoAsync(_dto).Wait();
            _dtos = Database.GetDataProviderInfosAsync().Result.ToArray();
        }

        [Fact]
        public void It_should_save_data_provider_with_status_fetch() => 
            _dtos.Single(x => x.DataProviderInfoId == _dto.DataProviderInfoId).Status.Should().Be(LastFetchStatus.Fetching);

        [Fact]
        public void It_should_not_set_end_offset() =>
            _dtos.Single(x => x.DataProviderInfoId == _dto.DataProviderInfoId).LastFetchStop.Should().Be(null);
    }

    public class when_saving_data_provider_info_with_fetch_end: SmogInfrastructureSpec
    {
        private static DataProviderInfoDto _dto;
        private static DataProviderInfoDto[] _dtos;
        public when_saving_data_provider_info_with_fetch_end()
        {
            _dto = DataProviderInfoDto.CreateNew("provider", DateTimeOffset.Now);
            Database.SaveDataProviderInfoAsync(_dto).Wait();
            _dto.Status.Should().Be(LastFetchStatus.Fetching);
           _dto.SetFetchEnd(DateTimeOffset.Now.AddHours(1),LastFetchStatus.Error,"It was an error.");
            Database.SaveDataProviderInfoAsync(_dto).Wait();
            _dtos = Database.GetDataProviderInfosAsync().Result.ToArray();
        }

        [Fact]
        public void It_should_save_data_provider_with_fetch_status_and_end_offset() { 
            var dto = _dtos.Single(x => x.DataProviderInfoId == _dto.DataProviderInfoId);
            dto.Status.Should().Be(LastFetchStatus.Error);
            dto.Info.Should().NotBeEmpty();
            dto.LastFetchStop.Should().Be(_dto.LastFetchStop);
            _dto.LastFetchStop.Value.Subtract(_dto.LastFetchStart).TotalHours.Should().BeGreaterThan(1);
        }
    }

  
    public class when_getting_data_provider_infos: SmogInfrastructureSpec
    {
        public when_getting_data_provider_infos()
        {
            Database.SaveDataProviderInfoAsync(DataProviderInfoDto.CreateNew("provider1", DateTimeOffset.Now)).Wait();
            Database.SaveDataProviderInfoAsync(DataProviderInfoDto.CreateNew("provider2", DateTimeOffset.Now)).Wait();
            Database.SaveDataProviderInfoAsync(DataProviderInfoDto.CreateNew("provider3", DateTimeOffset.Now)).Wait();
        }
        [Fact]
        public void It_should_get_all_provider_infos()
        {
            var infos = Database.GetDataProviderInfosAsync().Result.ToArray();
            infos.Select(x => x.ProviderId).Should().Contain("provider1", "provider2", "provider3");
        }
    }

    public class when_getting_latest_successful_data_provider_info_for_provider_id_and_provider_does_not_have_success_info : SmogInfrastructureSpec
    {
        public when_getting_latest_successful_data_provider_info_for_provider_id_and_provider_does_not_have_success_info()
        {
            Database.SaveDataProviderInfoAsync(DataProviderInfoDto.CreateNew("provider1", DateTimeOffset.Now)).Wait();
        }

        [Fact]
        public void It_should_get_latest_successful_provider_info_for_provider_id()
        {
            var info = Database.GetLatestSuccessfulDataProviderInfoAsync("provider1").Result;
            info.Should().BeNull();
        }
    }

    public class when_getting_latest_successful_data_provider_info_for_provider_id : SmogInfrastructureSpec
    {
        private static DataProviderInfoDto _dto;
        public when_getting_latest_successful_data_provider_info_for_provider_id()
        {
            _dto = DataProviderInfoDto.CreateNew("provider1",DateTimeOffset.Now.AddDays(-1));
            _dto.SetFetchEnd(DateTimeOffset.Now.AddDays(-1).AddHours(1),LastFetchStatus.Ok,"Finished in 10 seconds..");
            Database.SaveDataProviderInfoAsync(_dto);
            Database.SaveDataProviderInfoAsync(DataProviderInfoDto.CreateNew("provider1", DateTimeOffset.Now));
            var other = DataProviderInfoDto.CreateNew("provider2", DateTimeOffset.Now);
            other.SetFetchEnd(DateTimeOffset.Now,LastFetchStatus.Error,"error");
            Database.SaveDataProviderInfoAsync(other).Wait();
        }

        [Fact]
        public void It_should_get_latest_successful_provider_info_for_provider_id()
        {
            var info = Database.GetLatestSuccessfulDataProviderInfoAsync("provider1").Result;
            info.Should().NotBe(null);
            info.DataProviderInfoId.Should().Be(_dto.DataProviderInfoId);
        }
    }
    public class when_saving_station_geo_data : SmogInfrastructureSpec
    {

        private static StationGeoDto _stationGeoDto;

        public when_saving_station_geo_data()
        {
       
            _stationGeoDto = new StationGeoDto(0.1, 0.2, "stationId","providerId",DateTimeOffset.Now);
            Database.SaveStationGeoAsync(_stationGeoDto).Wait();
        }

        [Fact]
        public void It_should_save_station_geo_data()
        {
            var stationGeos =
                Database.GetStationGeosAsync().Result.ToArray();
            stationGeos.Single().StationGeoDataId.Should().Be(_stationGeoDto.StationGeoDataId);
        }
    }

    public class when_saving_station_geo_data_and_data_already_exists : SmogInfrastructureSpec
    {
        public when_saving_station_geo_data_and_data_already_exists()
        {
            var old = new StationGeoDto(0.1, 0.2, "stationId","providerId", DateTimeOffset.Now.AddHours(-1));
            Database.SaveStationGeoAsync(old).Wait();
            Database.GetStationGeosAsync().Result.Single().Status.Should().BeNull();
            //owerwrite
            var newdto = new StationGeoDto(0.1,0.2,"stationId","providerId","street","streetNo","postalNo","city","state","country","formattedAddress",DateTimeOffset.Now);
            newdto.StationGeoDataId = old.StationGeoDataId;
            Database.SaveStationGeoAsync(newdto).Wait();
        }

        [Fact]
        public void It_should_overwrite_existing_data()
        {
            var stationGeo =
                Database.GetStationGeosAsync().Result.Single();
            stationGeo.Status.Should().Be("OK");
        }
    }

    public class when_getting_distinc_station_geo_data : SmogInfrastructureSpec
    {
        private static  StationGeoDto[] _distinct;
        public when_getting_distinc_station_geo_data()
        {

            Database.InsertSmogDataAsync(new SmogDto {SmogDataId ="1",ProviderId = "id1",Longitude = 0.1, Latitude = 0.2, StationId = "One"}).Wait();
            Database.InsertSmogDataAsync(new SmogDto { SmogDataId = "2", ProviderId = "id2", Longitude = 0.1, Latitude = 0.2, StationId = "One" }).Wait();
            Database.InsertSmogDataAsync(new SmogDto { SmogDataId = "3", ProviderId = "id3", Longitude = 0.1, Latitude = 0.2, StationId = "Two" }).Wait();
            _distinct = Database.GetDistinctSmogDataStationsWithLocation().Result.ToArray();
        }

        [Fact]
        public void It_return_distinct_station_ids_with_geo_locations()
        {
            _distinct.Length.Should().Be(2);
            
        }
    }

    public class when_getting_smog_details_for_station : SmogInfrastructureSpec
    {
        private static List<SmogDto> result;
        private static DateTimeOffset _current;
        public  when_getting_smog_details_for_station()
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            _current = DateTimeOffset.Now;
            var from = _current.AddDays(-30);
            for (int i = 0; i < (int) TimeSpan.FromDays(30).TotalHours; i++)
            {
                Database.InsertSmogDataAsync(new SmogDto
                {
                    ProviderId = "providerId",
                    StationId = "statId",
                    Altitude = 120,
                    Country = "SI",
                    CreatedTime = DateTime.Now,
                    FetchTime = DateTime.Now,
                    IntervalStart = from.AddHours(i),
                    IntervalEnd = from.AddHours(i + 1),
                    Longitude = 10,
                    Latitude = 20,
                    StationName = "stationName",
                    SmogDataId = Guid.NewGuid().ToString(),
                    TimeZoneInfoId = "si",
                    ResponseHash = "hashhuh",
                    Polutants =
                        Enumerable.Range(1, 10)
                            .Select(
                                polutantType =>
                                    Enumerable.Range(1, 3).Select(interval => new SmogPolutantDto(rnd.Next(10, 200),
                                        (SmogPolutantType) polutantType, (SmogPolutantInterval) interval,
                                        SmogPolutantUnit.MikroGramsPerCubicMetre))).SelectMany(x => x).ToArray()
                });
            }


        }

        [Fact]
        public void it_should_get_data_for_past_30_days()
        {
          var result =   Database.GetSmogDataDetailsAsync(StationKey.Create("statId", "providerId"), _current).Result;
            result.Count().Should().Be(30*24);
        }
    }
}

