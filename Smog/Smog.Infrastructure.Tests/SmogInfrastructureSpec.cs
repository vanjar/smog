﻿using System;
using System.IO;
using Smog.Infrastructure.DataAccess;
using Autofac;
using Marten;
using Smog.Infrastructure.Model;
using System.Linq;
using Serilog;
using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.Tests
{
    public class SmogInfrastructureSpec 
    {
        protected static ISmogDatabase Database;
        protected static IContainer Container;  
        
        protected SmogDto CreateSmogDto(string stationId,string providerId, DateTimeOffset intervalStart)
        {
            return new SmogDto(DateTime.Now, null, "sn", stationId, 1, 2, 3, "c", providerId, "h", intervalStart, intervalStart.AddHours(1), "tz", 
                new SmogPolutantDto(2.2, SmogPolutantType.SO2, SmogPolutantInterval.Hour, SmogPolutantUnit.MikroGramsPerCubicMetre));
        }

        private void DeleteAllData()
        {
            using (var session = Container.Resolve<IDocumentStore>().LightweightSession())
            {
                session.Query<SmogDto>().ToArray().Each(x => session.Delete(x));
                session.Query<DataProviderInfoDto>().ToArray().Each(x => session.Delete(x));
                session.Query<StationGeoDto>().ToArray().Each(x => session.Delete(x));
                session.SaveChanges();
            }
        }

        public SmogInfrastructureSpec()
        {
          
            var builder = new ContainerBuilder();
            builder.RegisterModule<InfrastructureModule>();
            builder.RegisterModule<DataAccessModule>();
            builder.RegisterInstance<DocumentStore>(DocumentStore.For(ds =>
            {
                ds.AutoCreateSchemaObjects = AutoCreate.All;
                ds.DdlRules.TableCreation = CreationStyle.DropThenCreate;
                ds.Connection("host=localhost;port=5432;database=smogtest;password=password1#;username=postgres");
                ds.Schema.Include<SmogMartenRegistry>();
            })).As<IDocumentStore>().SingleInstance();

            Container = builder.Build();
            Database = Container.Resolve<ISmogDatabase>();

            DeleteAllData();

            Directory.GetCurrentDirectory();
            
     
        }

    
    }
}
