﻿using System.Linq;
using FluentAssertions;
using Smog.Infrastructure.Services;
using Xunit;

namespace Smog.Infrastructure.Tests.Services
{
    public class MasonrySortExtensionSpec
    {
        public class when_sorting_for_masonry
        {
            string[] _input;
            string[] _output;

            public when_sorting_for_masonry()
            {
                _input =new[] {"a","b","c","d","e","f","g","h","i","j","k"};
                _output = _input.OrderByMasonry(3).ToArray();
            }

            [Fact]
            public void it_should_sort_by_column_count()
            {
                _output.Should().Equal("a", "d", "g", "j", "b", "e", "h", "k", "c", "f", "i");
            }
        }
    }
}
