﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Smog.Infrastructure.Services;
using Xunit;
// ReSharper disable InconsistentNaming
namespace Smog.Infrastructure.Tests.Services
{
    public class when_calculating_due_time_and_difference_between_new_start_and_last_end_is_less_than_interval_time
    {
        static int _dueTimeInMiliseconds;
        private static DateTimeOffset _lastSuccess =new DateTimeOffset(2016,11,19,21,15,0,TimeSpan.FromHours(1));
        private static DateTimeOffset _startTime = new DateTimeOffset(2016, 11, 19, 21, 55, 0, TimeSpan.FromHours(1));
        private static int _intervalInMinutes = 60;
        public when_calculating_due_time_and_difference_between_new_start_and_last_end_is_less_than_interval_time()
        {
            _dueTimeInMiliseconds = DataServiceHelper.CalculateDueTimeInMs(_lastSuccess, _startTime, _intervalInMinutes);
        }

        [Fact]
        public void It_should_return_miliseconds_till_next_full_hour_and_five_minutes()
        {
            _dueTimeInMiliseconds.Should().Be(10*60*1000); //10 minutes
        }
    }

    public class when_calculating_due_time_and_difference_between_new_start_and_last_end_is_more_or_equals_inteval_time
    {
        static int _dueTimeInMiliseconds;
        private static DateTimeOffset _lastSuccess = new DateTimeOffset(2016, 11, 19, 21, 15, 0, TimeSpan.FromHours(1));
        private static DateTimeOffset _startTime = new DateTimeOffset(2016, 11, 19, 22, 55, 0, TimeSpan.FromHours(1));
        private static int _intervalInMinutes = 60;
        public when_calculating_due_time_and_difference_between_new_start_and_last_end_is_more_or_equals_inteval_time()
        {
            _dueTimeInMiliseconds = DataServiceHelper.CalculateDueTimeInMs(_lastSuccess, _startTime, _intervalInMinutes);
        }

        [Fact]
        public void It_should_return_zero_miliseconds()
        {
            _dueTimeInMiliseconds.Should().Be(0); 
        }
    }

    
    public class when_calculating_due_time_and_difference_between_new_start_and_last_end_is_null
    {
        static int _dueTimeInMiliseconds;
        private static DateTimeOffset _startTime = new DateTimeOffset(2016, 11, 19, 22, 55, 0, TimeSpan.FromHours(1));
        public when_calculating_due_time_and_difference_between_new_start_and_last_end_is_null()
        {
            _dueTimeInMiliseconds = DataServiceHelper.CalculateDueTimeInMs(null, _startTime, 60);
        }

        [Fact]
        public void It_should_return_zero_miliseconds()
        {
            _dueTimeInMiliseconds.Should().Be(0);
        }
    }
}
