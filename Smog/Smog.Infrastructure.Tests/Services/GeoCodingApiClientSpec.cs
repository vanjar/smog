﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using FluentAssertions;
using Smog.Infrastructure.GeoCodingApi;
using Smog.Infrastructure.Model;
using Xunit;

namespace Smog.Infrastructure.Tests.Services
{
    public class GeoCodingApiClientSpec : SmogInfrastructureSpec
    {
        private static GeoCodingApiClient _geoCodingApi;

        public GeoCodingApiClientSpec()
        {
            _geoCodingApi = Container.Resolve<GeoCodingApiClient>();
        }

        public class when_getting_geo_coding_data : GeoCodingApiClientSpec
        {
            private static GoogleReverseGeoCodingResponse _geoCodingApiResponse;

            public when_getting_geo_coding_data()
            {
                _geoCodingApiResponse = _geoCodingApi.GetAsync(46.065497, 14.512704).Result;
            }

            [Fact]
            public void It_should_get_geo_coding_data()
            {
                _geoCodingApiResponse.Status.Should().Be("OK");
                _geoCodingApiResponse.Results.Count.Should().BeGreaterThan(0);
            }
        }
    }
}