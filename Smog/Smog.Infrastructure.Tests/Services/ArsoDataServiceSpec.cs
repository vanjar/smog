﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac.Core;
using Xunit;
using Autofac;
using FluentAssertions;
using Smog.Infrastructure.DataProviders;
using Smog.Infrastructure.DataProviders.Arso;
using Smog.Infrastructure.Model;
using Smog.Infrastructure.Services;

namespace Smog.Infrastructure.Tests.Services
{
    public class ArsoDataServiceSpec : SmogInfrastructureSpec
    {
        public ArsoDataServiceSpec()
        {
            var arsoDataService = Container.Resolve<SmogDataService<ArsoDataProvider>>();
            arsoDataService.TestInterval = null;
            arsoDataService.StartAsync(CancellationToken.None).Wait();
        }

        [Fact]
        public void It_should_start_service_and_fetch_data_after_period_and_store_it_to_database()
        {
            SmogDto[] smogDto = null;
            int i = 0;
            while (smogDto == null || smogDto.Length == 0 || i< 10)
            {
                Task.Delay(2000).Wait();
                smogDto = Database.GetSmogDataByProviderIdAsync(DataProvidersIds.Arso).Result.ToArray();
                i++;
            }
            smogDto.Should().NotBeNull();
            smogDto.Length.Should().BeGreaterOrEqualTo(13);
        }
    }
}
