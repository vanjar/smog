﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using FluentAssertions;
using Smog.Infrastructure.Model.Enums;
using Smog.Infrastructure.Services.Aqi;
using Xunit;

// ReSharper disable  InconsistentNaming
namespace Smog.Infrastructure.Tests.Services
{
    public class UsEpaAqiCalculatorSpec : SmogInfrastructureSpec
    {
        protected  static IAqiCalculator _aqiCalc;
        public UsEpaAqiCalculatorSpec()
        {
            _aqiCalc = Container.Resolve<IEnumerable<IAqiCalculator>>().Single(x=> x.Id == AqiCalculatorTypes.UsEpa);
        }

        public class when_calculating_aqi_for_O3_1h : UsEpaAqiCalculatorSpec
        {
            [Theory]
            [InlineData(0, null)]
            [InlineData(125, 101)]
            [InlineData(164, 150)]
            [InlineData(165, 151)]
            [InlineData(204, 200)]
            [InlineData(205, 201)]
            [InlineData(404, 300)]
            [InlineData(405, 301)]
            [InlineData(504, 400)]
            [InlineData(505, 401)]
            [InlineData(604, 500)]
            [InlineData(605, null)]
            public void it_should_calculate_aqi(double polutantValue, int? expectedAqi)
            {
                _aqiCalc.Calculate(SmogPolutantType.O3, SmogPolutantInterval.Hour,SmogPolutantUnit.MikroGramsPerCubicMetre,  polutantValue* 1.9957)
                    .Should()
                    .Be(expectedAqi);
            }
        }

        public class when_calculating_aqi_for_O3_8h : UsEpaAqiCalculatorSpec
        {
            [Theory]
            [InlineData(0, 0)]
            [InlineData(54, 50)]
            [InlineData(55, 51)]
            [InlineData(70, 100)]
            [InlineData(71, 101)]
            [InlineData(85, 150)]
            [InlineData(86, 151)]
            [InlineData(105, 200)]
            [InlineData(106, 201)]
            [InlineData(200, 300)]
            [InlineData(201, null)]
            public void it_should_calculate_aqi(double polutantValue, int? expectedAqi)
            {
                _aqiCalc.Calculate(SmogPolutantType.O3, SmogPolutantInterval.EightHour, SmogPolutantUnit.MikroGramsPerCubicMetre, polutantValue* 1.9957)
                    .Should()
                    .Be(expectedAqi);
            }
        }

        public class when_calculating_aqi_for_Pm25_24h : UsEpaAqiCalculatorSpec
        {
            [Theory]
            [InlineData(0, 0)]
            [InlineData(12, 50)]
            [InlineData(12.1, 51)]
            [InlineData(35.4, 100)]
            [InlineData(35.5, 101)]
            [InlineData(55.4, 150)]
            [InlineData(55.5, 151)]
            [InlineData(150.4, 200)]
            [InlineData(150.5, 201)]
            [InlineData(250.4, 300)]
            [InlineData(250.5, 301)]
            [InlineData(350.4, 400)]
            [InlineData(350.5, 401)]
            [InlineData(500.4, 500)]
            [InlineData(500.5, null)]
            public void it_should_calculate_aqi(double polutantValue, int? expectedAqi)
            {
                _aqiCalc.Calculate(SmogPolutantType.Pm2Point5, SmogPolutantInterval.TwentyFourHour, SmogPolutantUnit.MikroGramsPerCubicMetre, polutantValue)
                    .Should()
                    .Be(expectedAqi);
            }
        }

        public class when_calculating_aqi_for_Pm10_24h : UsEpaAqiCalculatorSpec
        {
            [Theory]
            [InlineData(0, 0)]
            [InlineData(54, 50)]
            [InlineData(55, 51)]
            [InlineData(154, 100)]
            [InlineData(155, 101)]
            [InlineData(254, 150)]
            [InlineData(255, 151)]
            [InlineData(354, 200)]
            [InlineData(355, 201)]
            [InlineData(424, 300)]
            [InlineData(425, 301)]
            [InlineData(504, 400)]
            [InlineData(505, 401)]
            [InlineData(604, 500)]
            [InlineData(605, null)]
            public void it_should_calculate_aqi(double polutantValue, int? expectedAqi)
            {
                _aqiCalc.Calculate(SmogPolutantType.Pm10, SmogPolutantInterval.TwentyFourHour, SmogPolutantUnit.MikroGramsPerCubicMetre, polutantValue)
                    .Should()
                    .Be(expectedAqi);
            }
        }

        public class when_calculating_aqi_for_CO_8h : UsEpaAqiCalculatorSpec
        {
            [Theory]
            [InlineData(0, 0)]
            [InlineData(4.4, 50)]
            [InlineData(4.5, 51)]
            [InlineData(9.4, 100)]
            [InlineData(9.5, 101)]
            [InlineData(12.4, 150)]
            [InlineData(12.5, 151)]
            [InlineData(15.4, 200)]
            [InlineData(15.5, 201)]
            [InlineData(30.4, 300)]
            [InlineData(30.5, 301)]
            [InlineData(40.4, 400)]
            [InlineData(40.5, 401)]
            [InlineData(50.4, 500)]
            [InlineData(50.5, null)]
            public void it_should_calculate_aqi(double polutantValue, int? expectedAqi)
            {
                _aqiCalc.Calculate(SmogPolutantType.CO, SmogPolutantInterval.EightHour, SmogPolutantUnit.MiliGramsPerCubicMetre, polutantValue * 1.1642)
                    .Should()
                    .Be(expectedAqi);
            }
        }

        public class when_calculating_aqi_for_SO2_1hr : UsEpaAqiCalculatorSpec
        {
            [Theory]
            [InlineData(0, 0)]
            [InlineData(35, 50)]
            [InlineData(36, 51)]
            [InlineData(75, 100)]
            [InlineData(76, 101)]
            [InlineData(185, 150)]
            [InlineData(186, 151)]
            [InlineData(304, 200)]
            [InlineData(305, null)]
        
            public void it_should_calculate_aqi(double polutantValue, int? expectedAqi)
            {
                _aqiCalc.Calculate(SmogPolutantType.SO2, SmogPolutantInterval.Hour, SmogPolutantUnit.MikroGramsPerCubicMetre, polutantValue* 2.6609)
                    .Should()
                    .Be(expectedAqi);
            }
        }
        
        public class when_calculating_aqi_for_SO2_24hr : UsEpaAqiCalculatorSpec
        {
            [Theory]
            [InlineData(304, null)]
            [InlineData(305, 201)]
            [InlineData(604, 300)]
            [InlineData(605, 301)]
            [InlineData(804, 400)]
            [InlineData(805, 401)]
            [InlineData(1004, 500)]
            [InlineData(1005, null)]

            public void it_should_calculate_aqi(double polutantValue, int? expectedAqi)
            {
                _aqiCalc.Calculate(SmogPolutantType.SO2, SmogPolutantInterval.TwentyFourHour, SmogPolutantUnit.MikroGramsPerCubicMetre, polutantValue* 2.6609)
                    .Should()
                    .Be(expectedAqi);
            }
        }

        public class when_calculating_aqi_for_NO2_1h : UsEpaAqiCalculatorSpec
        {
            [Theory]
            [InlineData(0, 0)]
            [InlineData(53, 50)]
            [InlineData(54, 51)]
            [InlineData(100, 100)]
            [InlineData(101, 101)]
            [InlineData(360, 150)]
            [InlineData(361, 151)]
            [InlineData(649, 200)]
            [InlineData(650, 201)]
            [InlineData(1249, 300)]
            [InlineData(1250, 301)]
            [InlineData(1649, 400)]
            [InlineData(1650, 401)]
            [InlineData(2049, 500)]
            [InlineData(2050, null)]
            public void it_should_calculate_aqi(double polutantValue, int? expectedAqi)
            {
                _aqiCalc.Calculate(SmogPolutantType.NO2, SmogPolutantInterval.Hour, SmogPolutantUnit.MikroGramsPerCubicMetre, polutantValue* 1.9125)
                    .Should()
                    .Be(expectedAqi);
            }
        }
    }
}
