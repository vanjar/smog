﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Smog.Infrastructure.Services;
using Autofac;
using Autofac.Core;
using Smog.Infrastructure.Cache;
using Smog.Infrastructure.Model;
using Xunit;
using FluentAssertions;

namespace Smog.Infrastructure.Tests.Services
{
    public class SmogCacheSpec : SmogInfrastructureSpec
    {
        private static SmogCache _smogCache;
        private static SmogDto _alreadyInCache;
        private static SmogCacheService _smogCacheService;
        public SmogCacheSpec()
        {
            _smogCacheService = Container.Resolve<SmogCacheService>();
            _smogCache = Container.Resolve<SmogCache>();
            _smogCache.Clear();
            _smogCacheService.TestInterval = null;
        }

        public class when_starting_smog_service : SmogCacheSpec
        {

            public when_starting_smog_service()
            {
                Database.InsertSmogDataAsync(CreateSmogDto("id1", "pId", DateTime.Now)).Wait();
                Database.InsertSmogDataAsync(CreateSmogDto("id1", "pId", DateTime.Now.AddDays(-1))).Wait();
                Database.InsertSmogDataAsync(CreateSmogDto("id2", "pId",  DateTime.Now)).Wait();
                Database.InsertSmogDataAsync(CreateSmogDto("id2", "pId", DateTime.Now.AddHours(1))).Wait();
                _alreadyInCache = CreateSmogDto("id4", "pId", DateTime.Now.AddDays(-3));
                _smogCache.UpdateSmogData(StationKey.Create(_alreadyInCache.StationId,_alreadyInCache.ProviderId), new List<SmogDto> {_alreadyInCache});
                _smogCacheService.StartAsync(CancellationToken.None).Wait();
                Task.Delay(500).Wait();
            }

            [Fact]
            public void It_should_refresh_cache_for_all_stations()
            {
                _smogCache.GetSmogData(StationKey.Create("id1", "pId")).Count.Should().Equals(2);
                _smogCache.GetSmogData(StationKey.Create("id2", "pId")).Count.Should().Equals(2);
            }
     
        }
    }
}
