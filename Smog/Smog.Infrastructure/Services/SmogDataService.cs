﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Smog.Infrastructure.DataAccess;
using Smog.Infrastructure.DataProviders;
using Smog.Infrastructure.Model;

namespace Smog.Infrastructure.Services
{
    public class SmogDataService<TDataProvider> :IService,ITestableDataService,ISmogDataService where TDataProvider : ISmogDataProvider
    {
        private readonly SmogDatabase _database;
        private readonly ISmogDataProvider _smogDataProvider;

        private Timer _timer;
        private bool _isRunning;
        private readonly object _syncRoot = new object();

        public int? TestInterval { get; set; }

        public ISmogDataProvider SmogDataProvider => _smogDataProvider;

        public SmogDataService(SmogDatabase database, TDataProvider smogDataProvider) 
        {
            _database = database;
            _smogDataProvider = smogDataProvider;
            _timer = new Timer(OnTimerElapsed,null,Timeout.Infinite,0);
        }

        public async Task<ServiceStartResult> StartAsync(CancellationToken cancellationToken)
        {
            Log.Information($"Starting SmogDataService<{_smogDataProvider.GetType().Name}>.");
            var latestProviderInfo =
                await _database.GetLatestSuccessfulDataProviderInfoAsync(_smogDataProvider.ProviderId);
            var dueTimeInMs = DataServiceHelper.CalculateDueTimeInMs(latestProviderInfo?.LastFetchStop,
                DateTimeOffset.Now, _smogDataProvider.IntervalInMinutes);
            Log.Information($"SmogDataService<{_smogDataProvider.GetType().Name}> fetch will be delayed for {TimeSpan.FromMilliseconds(dueTimeInMs).TotalMinutes} minutes.");
            _timer.Change(dueTimeInMs, TestInterval ?? (int)TimeSpan.FromMinutes(_smogDataProvider.IntervalInMinutes).TotalMilliseconds);
            return ServiceStartResult.Continue;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Log.Information($"Stopping SmogDataService<{_smogDataProvider.GetType().Name}>.");
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }
            return Task.FromResult(true);
        }

        private void OnTimerElapsed(object state)
        {
            lock (_syncRoot)
            {
                if (_timer == null || _isRunning)
                    return;
                
                _isRunning = true;
            }
            var dataProviderInfoDto = DataProviderInfoDto.CreateNew(_smogDataProvider.ProviderId, DateTimeOffset.Now);
            try
            {
                Log.Logger.Information($"SmogDataService<{_smogDataProvider.GetType().Name}>.OnTimerElapsed() start.");
               
                _database.SaveDataProviderInfoAsync(dataProviderInfoDto).Wait();
                var smogDtos = _smogDataProvider.GetDataAsync().Result.ToArray();
                smogDtos.Each(dto => _database.InsertSmogDataAsync(dto).Wait());
                if (smogDtos.Any())
                    dataProviderInfoDto.SetFetchEnd(DateTimeOffset.Now, LastFetchStatus.Ok,$"Saved {smogDtos.Length} dtos.");
                else
                    dataProviderInfoDto.SetFetchEnd(DateTimeOffset.Now, LastFetchStatus.Warning,"Everything was OK, but no data was fetched.");
                _database.SaveDataProviderInfoAsync(dataProviderInfoDto).Wait();
                Log.Logger.Information($"SmogDataService<{_smogDataProvider.GetType().Name}>.OnTimerElapsed() stop.");
            }
            catch (Exception ex)
            {
                dataProviderInfoDto.SetFetchEnd(DateTimeOffset.Now, LastFetchStatus.Error, ex.Message);
                Log.Logger.Error(ex, $"Error SmogDataService<{_smogDataProvider.GetType().Name}>.OnTimerElapsed().");
            }
            finally
            {
                _isRunning = false;
            }
        }
    }
}
