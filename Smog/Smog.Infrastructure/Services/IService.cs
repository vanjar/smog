﻿using System.Threading;
using System.Threading.Tasks;
using Smog.Infrastructure.DataProviders;

namespace Smog.Infrastructure.Services
{
    public interface IService
    {
        Task<ServiceStartResult> StartAsync(CancellationToken cancellationToken);
        Task StopAsync(CancellationToken cancellationToken);
    }

    public interface ITestableDataService
    {
        int? TestInterval { get; set; }
    }

    public interface ISmogDataService
    {
        ISmogDataProvider SmogDataProvider { get; }
    }
}
