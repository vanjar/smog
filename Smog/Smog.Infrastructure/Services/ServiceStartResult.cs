﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smog.Infrastructure.Services
{
    public enum ServiceStartResult
    {
        Continue,
        Stop
    }
}
