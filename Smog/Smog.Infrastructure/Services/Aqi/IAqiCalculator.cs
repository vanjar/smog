using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.Services.Aqi
{
    public class AqiCategory : IAqiCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public AqiCategory(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }

    public interface IAqiCategory
    {
        int Id { get; set; }
        string Name { get; set; }
    }

    public interface IAqiCalculator
    {
        string Id { get; }
        string Description { get; }

        int? Calculate(SmogPolutantType polutantType, SmogPolutantInterval polutantInterval,
            SmogPolutantUnit polutantUnit, double polutantConcentration);

        IAqiCategory GetCategoryByAqi(int aqiIndex);
    }
}