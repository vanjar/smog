using System;
using System.Linq;
using System.Net;
using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.Services.Aqi.UsEpaAqi
{
    public class UsEpaAqiPolutantRange
    {

        private static readonly UsEpaAqiPolutantRange[] _ranges = {
            new UsEpaAqiPolutantRange(SmogPolutantType.O3, SmogPolutantInterval.EightHour,
            new[]
            {
                new UsEpaAqiConcentrationBreakpoint(0, 54, UsEpaAqiCategory.Good),
                new UsEpaAqiConcentrationBreakpoint(55, 70, UsEpaAqiCategory.Moderate),
                new UsEpaAqiConcentrationBreakpoint(71, 85, UsEpaAqiCategory.UnhealthyForSensitiveGroups),
                new UsEpaAqiConcentrationBreakpoint(86, 105, UsEpaAqiCategory.Unhealthy),
                new UsEpaAqiConcentrationBreakpoint(106, 200, UsEpaAqiCategory.VeryUnhealthy),
            }),
            new UsEpaAqiPolutantRange(SmogPolutantType.O3, SmogPolutantInterval.Hour,
                new[]
                {
                    new UsEpaAqiConcentrationBreakpoint(125, 164, UsEpaAqiCategory.UnhealthyForSensitiveGroups),
                    new UsEpaAqiConcentrationBreakpoint(165, 204, UsEpaAqiCategory.Unhealthy),
                    new UsEpaAqiConcentrationBreakpoint(205, 404, UsEpaAqiCategory.VeryUnhealthy),
                    new UsEpaAqiConcentrationBreakpoint(405, 504, UsEpaAqiCategory.HazardousLow),
                    new UsEpaAqiConcentrationBreakpoint(505, 604, UsEpaAqiCategory.HazardousHigh),
                }),
            new UsEpaAqiPolutantRange(SmogPolutantType.Pm2Point5, SmogPolutantInterval.TwentyFourHour,
                new[]
                {
                    new UsEpaAqiConcentrationBreakpoint(0.0, 12.0, UsEpaAqiCategory.Good),
                    new UsEpaAqiConcentrationBreakpoint(12.1, 35.4, UsEpaAqiCategory.Moderate),
                    new UsEpaAqiConcentrationBreakpoint(35.5, 55.4, UsEpaAqiCategory.UnhealthyForSensitiveGroups),
                    new UsEpaAqiConcentrationBreakpoint(55.5, 150.4, UsEpaAqiCategory.Unhealthy),
                    new UsEpaAqiConcentrationBreakpoint(150.5, 250.4, UsEpaAqiCategory.VeryUnhealthy),
                    new UsEpaAqiConcentrationBreakpoint(250.5, 350.4, UsEpaAqiCategory.HazardousLow),
                    new UsEpaAqiConcentrationBreakpoint(350.5, 500.4, UsEpaAqiCategory.HazardousHigh),
                }),
            new UsEpaAqiPolutantRange(SmogPolutantType.Pm10, SmogPolutantInterval.TwentyFourHour,
                new[]
                {
                    new UsEpaAqiConcentrationBreakpoint(0, 54, UsEpaAqiCategory.Good),
                    new UsEpaAqiConcentrationBreakpoint(55,154, UsEpaAqiCategory.Moderate),
                    new UsEpaAqiConcentrationBreakpoint(155,254, UsEpaAqiCategory.UnhealthyForSensitiveGroups),
                    new UsEpaAqiConcentrationBreakpoint(255, 354, UsEpaAqiCategory.Unhealthy),
                    new UsEpaAqiConcentrationBreakpoint(355, 424, UsEpaAqiCategory.VeryUnhealthy),
                    new UsEpaAqiConcentrationBreakpoint(425, 504, UsEpaAqiCategory.HazardousLow),
                    new UsEpaAqiConcentrationBreakpoint(505,604, UsEpaAqiCategory.HazardousHigh),
                }),
             new UsEpaAqiPolutantRange(SmogPolutantType.CO, SmogPolutantInterval.EightHour,
                new[]
                {
                    new UsEpaAqiConcentrationBreakpoint(0.0, 4.4, UsEpaAqiCategory.Good),
                    new UsEpaAqiConcentrationBreakpoint(4.5,9.4, UsEpaAqiCategory.Moderate),
                    new UsEpaAqiConcentrationBreakpoint(9.5,12.4, UsEpaAqiCategory.UnhealthyForSensitiveGroups),
                    new UsEpaAqiConcentrationBreakpoint(12.5, 15.4, UsEpaAqiCategory.Unhealthy),
                    new UsEpaAqiConcentrationBreakpoint(15.5, 30.4, UsEpaAqiCategory.VeryUnhealthy),
                    new UsEpaAqiConcentrationBreakpoint(30.5, 40.4, UsEpaAqiCategory.HazardousLow),
                    new UsEpaAqiConcentrationBreakpoint(40.5,50.4, UsEpaAqiCategory.HazardousHigh),
                }),
               new UsEpaAqiPolutantRange(SmogPolutantType.SO2, SmogPolutantInterval.Hour,
                new[]
                {
                    new UsEpaAqiConcentrationBreakpoint(0, 35, UsEpaAqiCategory.Good),
                    new UsEpaAqiConcentrationBreakpoint(36,75, UsEpaAqiCategory.Moderate),
                    new UsEpaAqiConcentrationBreakpoint(76,185, UsEpaAqiCategory.UnhealthyForSensitiveGroups),
                    new UsEpaAqiConcentrationBreakpoint(186, 304, UsEpaAqiCategory.Unhealthy),
                   
                }),
                 new UsEpaAqiPolutantRange(SmogPolutantType.SO2, SmogPolutantInterval.TwentyFourHour,
                new[]
                {
                    new UsEpaAqiConcentrationBreakpoint(305, 604, UsEpaAqiCategory.VeryUnhealthy),
                    new UsEpaAqiConcentrationBreakpoint(605, 804, UsEpaAqiCategory.HazardousLow),
                    new UsEpaAqiConcentrationBreakpoint(805,1004, UsEpaAqiCategory.HazardousHigh),
                }),
                    new UsEpaAqiPolutantRange(SmogPolutantType.NO2, SmogPolutantInterval.Hour,
                new[]
                {
                    new UsEpaAqiConcentrationBreakpoint(0, 53, UsEpaAqiCategory.Good),
                    new UsEpaAqiConcentrationBreakpoint(54,100, UsEpaAqiCategory.Moderate),
                    new UsEpaAqiConcentrationBreakpoint(101,360, UsEpaAqiCategory.UnhealthyForSensitiveGroups),
                    new UsEpaAqiConcentrationBreakpoint(361, 649, UsEpaAqiCategory.Unhealthy),
                    new UsEpaAqiConcentrationBreakpoint(650, 1249, UsEpaAqiCategory.VeryUnhealthy),
                    new UsEpaAqiConcentrationBreakpoint(1250, 1649, UsEpaAqiCategory.HazardousLow),
                    new UsEpaAqiConcentrationBreakpoint(1650,2049, UsEpaAqiCategory.HazardousHigh),
                }),
        };

        public SmogPolutantType PolutantType { get; private set; }
        public SmogPolutantInterval PolutantInterval { get; private set; }

        public UsEpaAqiConcentrationBreakpoint[] Breakpoints { get; private set; }

        public UsEpaAqiPolutantRange(SmogPolutantType polutantType,
            SmogPolutantInterval polutantInterval, UsEpaAqiConcentrationBreakpoint[] breakpoints)
        {
            PolutantType = polutantType;
            PolutantInterval = polutantInterval;
            Breakpoints = breakpoints;
        }

        public static UsEpaAqiConcentrationBreakpoint GetBreakpoint(SmogPolutantType polutantType,
            SmogPolutantInterval polutantInterval, double polutantContentration)
        {
            var breakpoints =_ranges.SingleOrDefault(r => r.PolutantInterval == polutantInterval && r.PolutantType == polutantType)?.Breakpoints;

            if (breakpoints == null)
                return null;

            polutantContentration = Math.Round(polutantContentration, 1);
            var breakpoint = breakpoints.SingleOrDefault(b =>
                    polutantContentration >= b.Low &&
                    polutantContentration <= b.High);

            return breakpoint;
        }
    }
}