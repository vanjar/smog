namespace Smog.Infrastructure.Services.Aqi.UsEpaAqi
{
    public class UsEpaAqiConcentrationBreakpoint
    {
        public double Low { get; private set; }
        public double High { get; private set; }
    
        public UsEpaAqiCategory AqiCategory { get; private set; }

        public UsEpaAqiConcentrationBreakpoint(double low, double high, UsEpaAqiCategory aqiCategory)
        {
            Low = low;
            High = high;
            AqiCategory = aqiCategory;
        } 
    }
}