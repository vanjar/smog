namespace Smog.Infrastructure.Services.Aqi.UsEpaAqi
{
    public enum UsEpaAqiCategory
    {
        Good = 0,
        Moderate = 1,
        UnhealthyForSensitiveGroups = 2,
        Unhealthy = 3,
        VeryUnhealthy = 4,
        HazardousLow = 5,
        HazardousHigh= 6
    }
}