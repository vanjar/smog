﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.Services.Aqi.UsEpaAqi
{
    public class UsEpaAqiCalculator : IAqiCalculator
    {
        public string Id =>  AqiCalculatorTypes.UsEpa;

        public string Description => "United States Environmental Protection Agency Air Quality Index";

        
        public UsEpaAqiCalculator()
        {

        }

        private bool IsUsEpaAqiInputPolutant(SmogPolutantType polutantType)
        {
            return new[]
            {
                SmogPolutantType.O3,
                SmogPolutantType.NO2,
                SmogPolutantType.CO,
                SmogPolutantType.SO2,
                SmogPolutantType.Pm10,
                SmogPolutantType.Pm2Point5,
            }.Any(pt => pt == polutantType);
        }

        private double ConvertToAqiUnit(SmogPolutantType polutantType,
            SmogPolutantUnit polutantUnit,
            double polutantConcentration)
        {
            if (polutantType == SmogPolutantType.O3 && polutantUnit == SmogPolutantUnit.MikroGramsPerCubicMetre)
                return polutantConcentration / 1.9957; //ppb
            if (polutantType == SmogPolutantType.NO2 && polutantUnit == SmogPolutantUnit.MikroGramsPerCubicMetre)
                return polutantConcentration / 1.9125; //ppb
            if (polutantType == SmogPolutantType.CO && polutantUnit == SmogPolutantUnit.MiliGramsPerCubicMetre)
                return polutantConcentration / 1.1642; //ppm
            if (polutantType == SmogPolutantType.SO2 && polutantUnit == SmogPolutantUnit.MikroGramsPerCubicMetre)
                return polutantConcentration / 2.6609; //ppb
            if (polutantType == SmogPolutantType.Pm10 && polutantUnit == SmogPolutantUnit.MikroGramsPerCubicMetre)
                return polutantConcentration;
            if (polutantType == SmogPolutantType.Pm2Point5 && polutantUnit == SmogPolutantUnit.MikroGramsPerCubicMetre)
                return polutantConcentration;

            throw new ArgumentException($"Polutant type {polutantType} and unit {polutantUnit} is not valid for US EPA AQI calculator input.");
        }

        public int? Calculate(SmogPolutantType polutantType, SmogPolutantInterval polutantInterval,SmogPolutantUnit polutantUnit,
            double polutantConcentration)
        {
            if (IsUsEpaAqiInputPolutant(polutantType))
                polutantConcentration = ConvertToAqiUnit(polutantType, polutantUnit, polutantConcentration);

            var concentrationBreakpoint = UsEpaAqiPolutantRange.GetBreakpoint(polutantType, polutantInterval,
                polutantConcentration);
            if (concentrationBreakpoint == null)
                return null;

            var indexBreakpoint = UsEpaAqiIndexRange.GetByCategory(concentrationBreakpoint.AqiCategory);
            
            double airqualityIndex = (indexBreakpoint.High - indexBreakpoint.Low) /
                                     (concentrationBreakpoint.High - concentrationBreakpoint.Low) *
                                     (polutantConcentration - concentrationBreakpoint.Low) + indexBreakpoint.Low;

            return (int)Math.Round(airqualityIndex,0);
        }

        public IAqiCategory GetCategoryByAqi(int aqi)
        {
            var useEpaCategory = UsEpaAqiIndexRange.GetAll().Single(i => aqi >= i.Low && aqi <= i.High);
            return new AqiCategory((int)useEpaCategory.Category, useEpaCategory.Name);
        }
    }
}
