using System.Linq;

namespace Smog.Infrastructure.Services.Aqi.UsEpaAqi
{
    public class UsEpaAqiIndexRange
    {
        private static UsEpaAqiIndexRange[] _indexes 
            = {
                new UsEpaAqiIndexRange(UsEpaAqiCategory.Good, 0,50,"Good"),
                new UsEpaAqiIndexRange(UsEpaAqiCategory.Moderate, 51,100,"Moderate"),
                new UsEpaAqiIndexRange(UsEpaAqiCategory.UnhealthyForSensitiveGroups, 101,150,"Unhealthy for sensitive groups"),
                new UsEpaAqiIndexRange(UsEpaAqiCategory.Unhealthy, 151,200,"Unhealthy"),
                new UsEpaAqiIndexRange(UsEpaAqiCategory.VeryUnhealthy,201,300,"Very unhealthy"),     
                new UsEpaAqiIndexRange(UsEpaAqiCategory.HazardousLow, 301,400,"Hazardous"),
                new UsEpaAqiIndexRange(UsEpaAqiCategory.HazardousHigh, 401,500,"Hazardous")
            };

        public UsEpaAqiCategory Category { get; }
        public double High { get; }
        public double Low { get;  }
        public string Name { get; }

        public UsEpaAqiIndexRange(UsEpaAqiCategory category, double low, double high, string name)
        {
            Category = category;
            High = high;
            Low = low;
            Name = name;
        }
        
        public static UsEpaAqiIndexRange[] GetAll()
        {
            return _indexes;
        }

        public static UsEpaAqiIndexRange GetByCategory(UsEpaAqiCategory category)
        {
            return _indexes.Single(x => x.Category == category);
        }

        public static double GetMaxIndexBreakpointHigh()
        {
            return _indexes.OrderBy(x => x.High).Last().High;
        }

    }
}