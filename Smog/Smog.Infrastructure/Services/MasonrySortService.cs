﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smog.Infrastructure.Services
{
    public static class MasonrySortExtension
    {
        //1 3 5
        //2 4 6
        
        //TO
        //1 2 3
        //4 5 6
        
        public static IEnumerable<T> OrderByMasonry<T>(this IEnumerable<T> ordered, int noOfcolumns)
        {
            var batches = ordered.Batch(noOfcolumns).ToArray();
            var result = Enumerable.Range(0,noOfcolumns)
                .SelectMany(i=> batches.Where(b=> b.Count() > i).Select(b=> b.ElementAt(i))
                .ToArray());
            return result;
        }
    }
}
