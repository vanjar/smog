﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Smog.Infrastructure.Cache;
using Smog.Infrastructure.DataAccess;
using Smog.Infrastructure.Model;

namespace Smog.Infrastructure.Services
{
 
    public class SmogCacheService : IService 
    {
        private readonly SmogDatabase _database;
        private readonly SmogCache _smogCache;


        private Timer _timer;
        private bool _isRunning;
        private readonly object _syncRoot = new object();

        public int? TestInterval { get; set; }

        public SmogCacheService(SmogDatabase database, SmogCache smogCache)
        {
            _database = database;
            _smogCache = smogCache;
            _timer = new Timer(OnTimerElapsed, null, Timeout.Infinite, 0);
        }

        public Task<ServiceStartResult> StartAsync(CancellationToken cancellationToken)
        {
            Log.Information($"{this.GetType().Name} starting.");
            _timer.Change(0, TestInterval ?? (int)TimeSpan.FromMinutes(1).TotalMilliseconds);
            return Task.FromResult(ServiceStartResult.Continue);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Log.Information($"{this.GetType().Name} stoping.");
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }
            return Task.FromResult(true);
        }

        private void OnTimerElapsed(object state)
        {
            lock (_syncRoot)
            {
                if (_timer == null || _isRunning)
                    return;

                _isRunning = true;
            }
            try
            {
                Log.Information($"{this.GetType().Name}.OnTimerElapsed() start.");
                InitSmogDataCache().Wait();
                InitStationGeoDataCache().Wait();
                Log.Information($"{this.GetType().Name}.OnTimerElapsed() stop.");
            }
            catch (Exception ex)
            {
                Log.Error(ex,$"{this.GetType().Name}.OnTimerElapsed() error.");
            }
            finally
            {
                _isRunning = false;
            }
        }

        private async Task InitSmogDataCache()
        {
            Dictionary<string, List<SmogDto>> latestPast24 = await _database.GetLatestPast24HourSmogDataAsync();
            if (latestPast24 == null)
            {
                Log.Information($"Smog Data cache could not be initialized. Can't get latest past 24h data.");
                return;
            }
            latestPast24.Each(d=> _smogCache.UpdateSmogData(d.Key,d.Value));
            Log.Information($"Cache initialized.");
        }

        private async Task InitStationGeoDataCache()
        {
           var geos = (await _database.GetStationGeosAsync()).ToArray();
            if (geos == null || !geos.Any())
            {
                Log.Information($"Station geo data cache could not be initialized.");
                return;
            }
            geos.Each(d => _smogCache.UpdateStationGeoData(StationKey.Create(d.StationId,d.ProviderId), d));
            Log.Information($"Cache initialized.");
        }

    }
}
