﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smog.Infrastructure.Services
{
    public static class DataServiceHelper
    {
        public static int CalculateDueTimeInMs(DateTimeOffset? lastSuccessEndTime,DateTimeOffset startTimeOffset, int intervalInMinutes)
        {
            if (lastSuccessEndTime == null)
                return 0;
            var timeOfDay = startTimeOffset;
            var diff = timeOfDay.Subtract(lastSuccessEndTime.Value).TotalMinutes;
            if (diff >= intervalInMinutes)
                return 0;
            var nextFullHour =
                TimeSpan.FromHours(Math.Ceiling(timeOfDay.DateTime.TimeOfDay.TotalHours)).Add(TimeSpan.FromMinutes(5));
            return (int)(nextFullHour - timeOfDay.DateTime.TimeOfDay).TotalMilliseconds;
        }
    }
}
