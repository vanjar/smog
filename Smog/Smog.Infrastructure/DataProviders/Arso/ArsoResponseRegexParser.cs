﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Smog.Infrastructure.DataProviders.Arso
{
    public class ArsoResponseRegexParser
    {

        public ArsoResponseRegexParser()
        {
        }

        public ArsoDto ParseArsoResponse(string arsoResponse)
        {
            List<ArsoStationDto> arsoStationsDtos = new List<ArsoStationDto>();

            var createdOn = DateTime.Parse(Regex.Match(arsoResponse, @"<datum_priprave>(.*)</datum_priprave>").Groups[1].Value); 
            var matches = Regex.Matches(arsoResponse, @"(<postaja.*>.*</postaja>)+");
            for (int i = 0; i < matches.Count; i++)
                arsoStationsDtos.Add(CreateArsoStationDtoItemFromMatchGroup(matches[i].ToString()));

            return new ArsoDto
            {
                StationsData = arsoStationsDtos,
                CreatedOn = createdOn
            };
        }

        private static ArsoStationDto CreateArsoStationDtoItemFromMatchGroup(string stationGroup)
        {
            var attrsMatches = Regex.Match(stationGroup,
                "<postaja sifra=\"(.*)\" ge_dolzina=\"(.*)\" ge_sirina=\"(.*)\" nadm_visina=\"(.*)\">");

            return new ArsoStationDto
            {
                Name = Regex.Match(stationGroup, "<merilno_mesto>(.*)</merilno_mesto>").Groups[1].Value,
                IntervalStart = DateTime.Parse(Regex.Match(stationGroup, "<datum_od>(.*)</datum_od>").Groups[1].Value),
                IntervalEnd = DateTime.Parse(Regex.Match(stationGroup, "<datum_do>(.*)</datum_do>").Groups[1].Value),
                SO2 = ParseMeasurementValue(stationGroup, "so2"),
                CO = ParseMeasurementValue(stationGroup, "co"),
                O3 = ParseMeasurementValue(stationGroup, "o3"),
                NO2 = ParseMeasurementValue(stationGroup, "no2"),
                PM10 = ParseMeasurementValue(stationGroup, "pm10"),
                Id = attrsMatches.Groups[1].Value,
                Lon = double.Parse(attrsMatches.Groups[2].Value, new CultureInfo("en-US")),
                Lat = double.Parse(attrsMatches.Groups[3].Value, new CultureInfo("en-US")),
                Altitude = double.Parse(attrsMatches.Groups[4].Value, new CultureInfo("en-US"))
            };
        }

        private static double? ParseMeasurementValue(string station, string tagName)
        {
            var valueMatchResult = Regex.Match(station, $@"<{tagName}>(.*)</{tagName}>");
            if (!valueMatchResult.Success)
                return null;

            var val = valueMatchResult.Groups[1].Value.Replace("&lt;", "");
            if (string.IsNullOrEmpty(val))
                return null;
            return double.Parse(val, new CultureInfo("en-US"));
        }
    }
}
