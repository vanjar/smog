﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Baseline;
using Serilog;
using Smog.Infrastructure.Model;
using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.DataProviders.Arso
{
    public class ArsoDataProvider : ISmogDataProvider
    {
        private readonly ArsoResponseRegexParser _arsoResponseRegexParser;
        private readonly ResponseHasher _hasher;

        public string ProviderName => "Agencija Republike Slovenije za okolje";
        public string ProviderId => DataProvidersIds.Arso;
        public int IntervalInMinutes => 60;
        public TimeZoneInfo TimeZoneInfo => TimeZoneInfo.GetSystemTimeZones()[49];
        public string Country => "sl-SI";
        private HttpClient _httpClient;
        public ArsoDataProvider(ArsoResponseRegexParser arsoResponseRegexParser, ResponseHasher hasher)
        {
            _arsoResponseRegexParser = arsoResponseRegexParser;
            _hasher = hasher;
            _httpClient = new HttpClient();
        }

        public async Task<IEnumerable<SmogDto>> GetDataAsync()
        {
            var functionName = $"{this.GetType().Name}.GetDataAsync()";
            Log.Debug($"{functionName} started.");
            var arsoDailyDto = await GetDailyDataAsync().ConfigureAwait(false);
            var arsoHourlyDto = await GetHourlyDataAsync().ConfigureAwait(false);
            
            var smogdto = arsoHourlyDto.StationsData.Select(d =>
            {
                return ConvertToSmogDto(
                    d,
                    arsoDailyDto.postaja.SingleOrDefault(p => p.sifra.EqualsIgnoreCase(d.Id)),
                    DateTime.Now,
                    arsoHourlyDto.CreatedOn);
            }).ToArray();
            Log.Debug($"{functionName} finished.");
            return smogdto;
        }

        private async Task<arsopodatki> GetDailyDataAsync()
        {
            var httpResponse = await _httpClient.GetAsync("http://www.arso.gov.si/xml/zrak/ones_zrak_dnevni_podatki_zadnji.xml", CancellationToken.None);
            httpResponse.EnsureSuccessStatusCode();
            
            var response = await httpResponse.Content.ReadAsStringAsync();
            XmlSerializer serializer = new XmlSerializer(typeof(arsopodatki));
            arsopodatki dailyDto;
            using (TextReader reader = new StringReader(response))
            {
                dailyDto = (arsopodatki)serializer.Deserialize(reader);
            }
            if (dailyDto == null)
                throw new Exception($"Arso daily data is null.");
            Log.Debug($"Arso daily data finished with stations: {string.Join(",", dailyDto.postaja.Select(p=> p.merilno_mesto).ToArray())}");
            return dailyDto;
        }


        private async Task<ArsoDto> GetHourlyDataAsync()
        {
            var httpResponse = await _httpClient.GetAsync("http://www.arso.gov.si/xml/zrak/ones_zrak_urni_podatki_zadnji.xml", CancellationToken.None);
            httpResponse.EnsureSuccessStatusCode();

            var response = await httpResponse.Content.ReadAsStringAsync();

            var arsoDto = _arsoResponseRegexParser.ParseArsoResponse(response);
            if (arsoDto == null)
                throw new Exception($"Arso hourly data parser result is null.");
            Log.Debug($"Arso hourly data finished with stations: {string.Join(",", arsoDto?.StationsData.Select(x => x.Name).Distinct().ToArray())}");
            return arsoDto;
        }

        private double? ParseDouble(string pattern)
        {
            if (string.IsNullOrEmpty(pattern))
                return null;
            pattern = pattern.Replace("<", "");
            double d;
            if (double.TryParse(pattern,NumberStyles.Any, new CultureInfo("en-US"), out d))
                return d;
            return null;
        }

        private SmogDto ConvertToSmogDto(ArsoStationDto hourlySourceDto,arsopodatkiPostaja dailySourceDto, DateTime fetchTime,
            DateTime createdTime)
        {
            List<SmogPolutantDto> polutants = new List<SmogPolutantDto>();
            if (hourlySourceDto.SO2 != null)
                polutants.Add(new SmogPolutantDto(hourlySourceDto.SO2.Value,SmogPolutantType.SO2, SmogPolutantInterval.Hour, SmogPolutantUnit.MikroGramsPerCubicMetre));
            if (hourlySourceDto.O3 != null)
                polutants.Add(new SmogPolutantDto(hourlySourceDto.O3.Value, SmogPolutantType.O3, SmogPolutantInterval.Hour, SmogPolutantUnit.MikroGramsPerCubicMetre));
            if (hourlySourceDto.CO != null)
                polutants.Add(new SmogPolutantDto(hourlySourceDto.CO.Value, SmogPolutantType.CO, SmogPolutantInterval.Hour, SmogPolutantUnit.MiliGramsPerCubicMetre));
            if (hourlySourceDto.NO2 != null)
                polutants.Add(new SmogPolutantDto(hourlySourceDto.NO2.Value, SmogPolutantType.NO2, SmogPolutantInterval.Hour, SmogPolutantUnit.MikroGramsPerCubicMetre));
            if (hourlySourceDto.PM10 != null)
                polutants.Add(new SmogPolutantDto(hourlySourceDto.PM10.Value, SmogPolutantType.Pm10, SmogPolutantInterval.Hour, SmogPolutantUnit.MikroGramsPerCubicMetre));

            if (dailySourceDto != null)
            {
                var coMax8H = ParseDouble(dailySourceDto.co_max_8urna);
                if (coMax8H != null)
                    polutants.Add(new SmogPolutantDto(coMax8H.Value,SmogPolutantType.CO, SmogPolutantInterval.EightHour, SmogPolutantUnit.MiliGramsPerCubicMetre));
                
                var o3Max8H = ParseDouble(dailySourceDto.o3_max_8urna);
                if (o3Max8H != null)
                    polutants.Add(new SmogPolutantDto(o3Max8H.Value,SmogPolutantType.O3, SmogPolutantInterval.EightHour, SmogPolutantUnit.MikroGramsPerCubicMetre));

                var pm10Daily = ParseDouble(dailySourceDto.pm10_dnevna);
                if (pm10Daily != null)
                    polutants.Add(new SmogPolutantDto(pm10Daily.Value,SmogPolutantType.Pm10, SmogPolutantInterval.TwentyFourHour, SmogPolutantUnit.MikroGramsPerCubicMetre));

                var so2Daily = ParseDouble(dailySourceDto.so2_dnevna);
                if (so2Daily != null)
                    polutants.Add(new SmogPolutantDto(so2Daily.Value,SmogPolutantType.SO2, SmogPolutantInterval.TwentyFourHour, SmogPolutantUnit.MikroGramsPerCubicMetre));
                
            }
            if (polutants.Count == 0)
                Log.Debug($"No measurment values.");

            return new SmogDto(
                fetchTime,
                createdTime,
                hourlySourceDto.Name,
                hourlySourceDto.Id,
                hourlySourceDto.Lon,
                hourlySourceDto.Lat,
                hourlySourceDto.Altitude, 
                Country,
                ProviderId, 
                "",
                TimeZoneInfo.ConvertTime(hourlySourceDto.IntervalStart, TimeZoneInfo),
                TimeZoneInfo.ConvertTime(hourlySourceDto.IntervalEnd, TimeZoneInfo),
                TimeZoneInfo.Id, 
                polutants.ToArray());
        }
    }
}


