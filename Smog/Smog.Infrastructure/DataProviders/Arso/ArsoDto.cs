using System;
using System.Collections.Generic;

namespace Smog.Infrastructure.DataProviders.Arso
{

    public class ArsoStationDto
    {
        public string Id { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
        public double Altitude { get; set; }
        public string Name { get; set; }
        public DateTime IntervalStart { get; set; }
        public DateTime IntervalEnd { get; set; }
        public double? SO2 { get; set; }
        public double? CO { get; set; }
        public double? O3 { get; set; }
        public double? NO2 { get; set; }
        public double? PM10 { get; set; }
    }

    public class ArsoDto
    {
        public List<ArsoStationDto> StationsData { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}

