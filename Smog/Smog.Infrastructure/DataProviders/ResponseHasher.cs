﻿using System.Security.Cryptography;
using System.Text;

namespace Smog.Infrastructure.DataProviders
{
    public class ResponseHasher
    {
        private readonly MD5 _md5Hash;

        public string ComputeHash(string input)
        {
            var data = _md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();
            foreach (var t in data)
                sBuilder.Append(t.ToString("x2"));
            return sBuilder.ToString();
        }

        public ResponseHasher()
        {
            _md5Hash = MD5.Create();
        }
    }
}
