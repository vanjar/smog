﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Smog.Infrastructure.DataProviders.Azohr
{
    public enum AzoHrDataType
    {
        SatniIzvorni = 0,
        OsamSatniIzvorni = 2,
        DnevniIzvorni = 4
    }

    public enum AzoHrPolutant
    {
        SO2 = 2,
        O3 = 31,
        CO = 3,
        NO2 = 1,
        Pm10 = 5,
        H2S = 4,
        Benzene = 32,
        Methane = 71,
        NH3 = 95,
        Pm2Point5 = 28
    }

    public class AzoHrMetaData
    {
        private Dictionary<int, AzoHrStation> _azoHrStations;

        private static string _rawData =
            "DESINIĆ\tRH0112\t46,169056\t15,660639\t256\r\nHUM (otok Vis)\tRH0118\t43,031333\t16,115972\t262\r\nKARLOVAC-1\tRH0124\t45,493525\t15,565131\t278\r\nKOPAČKI RIT\tRH0111\t45,698028\t18,834639\t255\r\nKUTINA-1\tRH0105\t45,479056\t16,780667\t161\r\nOPUZEN (Delta Neretve)\tRH0119\t43,008728\t17,566025\t263\r\nOSIJEK-1\tRH0104\t45,558792\t18,698769\t160\r\nPARG\tRH0114\t45,593481\t14,630464\t258\r\nPLITVIČKA JEZERA\tRH0113\t44,899333\t15,609778\t257\r\nPOLAČA (Ravni kotari)\tRH0117\t44,021094\t15,516111\t261\r\nRIJEKA-1\tRH0107\t45,3275\t14,446633\t158\r\nRIJEKA-2\tRH0108\t45,320794\t14,483511\t159\r\nSISAK-1\tRH0106\t45,458125\t16,388356\t162\r\nSLAVONSKI BROD - privremena pokretna postaja\tRH0110\t45,148197\t18,023147\t254\r\nSLAVONSKI BROD-1\tRH0109\t45,159472\t17,9951\t165\r\nSLAVONSKI BROD-2\tRH0122\t45,149114\t18,02345\t275\r\nVARAŽDIN-1\tRH0123\t46,282911\t16,363803\t277\r\nVELA STRAŽA (Dugi otok)\tRH0116\t43,9915\t15,058111\t260\r\nVIŠNJAN\tRH0115\t45,291203\t13,749778\t259\r\nZAGREB PPI PM2,5 - Ksaverska cesta\tRH0121\t45,834372\t15,978394\t267\r\nZAGREB-1\tRH0101\t45,800339\t15,974072\t155\r\nZAGREB-2\tRH0102\t45,823717\t16,035825\t156\r\nZAGREB-3\tRH0103\t45,764947\t16,006469\t157\r\nŽARKOVICA (Dubrovnik)\tRH0120\t42,644697\t18,122611\t264\r\nVatrogasni dom (K2) - Kutina\tSM0101\t45,472778\t16,785\t273\r\nAMS1 Kaštel Sućurac\tSD0201\t43,548119\t16,434961\t173\r\nAMS2 Sveti Kajo\tSD0202\t43,545906\t16,467836\t179\r\nSplit-1\tSD0203\t43,510653\t16,449342\t168\r\nViškovo - Marišćina\tPG0401\t45,402222\t14,388333\t232\r\nKsaverska cesta\tGZ0201\t45,834381\t15,978156\t41\r\nPešćenica\tGZ0203\t45,804722\t16,032778\t102\r\nPrilaz baruna Filipovića\tGZ0204\t45,812222\t15,948611\t103\r\nSiget (Dom zdravlja)\tGZ0206\t45,773611\t15,984444\t119\r\nSusedgrad (Tvornica \"Utenzilija\")\tGZ0205\t45,812222\t15,873611\t118\r\nZagreb - Đorđićeva ulica (Stanica za hitnu pomoć)\tGZ0202\t45,811389\t15,989167\t101\r\nZagreb - Jakuševec\tGZ0207\t45,763728\t16,018014\t274\r\nKrasica\tPG0303\t45,308333\t14,551667\t243\r\nPaveki\tPG0302\t45,294167\t14,513889\t224\r\nUrinj\tPG0301\t45,288611\t14,528333\t221\r\nVrh Martinšćice\tPG0304\t45,311389\t14,487222\t246\r\nSisak 2 Galdovo\tSM0301\t45,477961\t16,399617\t151\r\nViškovo - Viševac\tPG0201\t45,368889\t14,383889\t235\r\nKrešimirova ul.\tPG0101\t45,331667\t14,425556\t245\r\nMlaka\tPG0103\t45,338889\t14,4125\t276\r\nOpatija - Gorovo\tPG0102\t45,336667\t14,306667\t241\r\nKlavar\tIS0504\t45,134306\t14,167167\t191\r\nPlomin\tIS0503\t45,139583\t14,179139\t190\r\nRipenda\tIS0501\t45,10975\t14,138972\t180\r\nSv. Katarina\tIS0502\t45,173444\t14,045333\t189\r\nKostrena - Martinšćica\tPG0501\t45,313333\t14,482778\t215\r\nKoromačno\tIS0301\t44,980422\t14,122108\t40\r\nPula Fižela\tIS0201\t44,862469\t13,816858\t39\r\nVelika Gorica\tZA0201\t45,714847\t16,068289\t121\r\nVrhovec\tGZ0101\t45,817961\t15,94795\t38\r\nZagreb - Bijenik\tGZ0102\t45,846775\t15,939175\t265\r\nZoljan\tOB0101\t45,472442\t18,039125\t37\r\nČambarelići\tIS0102\t45,181331\t14,101011\t20\r\nZajci\tIS0101\t45,204147\t14,070761\t14\r\nSisak 3\tSM0201\t45,488956\t16,373778\t133\r\nPleso\tZA0101\t45,746619\t16,080225\t279";

        private Dictionary<int, string> _dataTypes;
        private Dictionary<int, string> _polutantTypes;
        private int[] _hourlyPolutantTypes;
        private int[] _eightHoursPolutantTypes;
        private Dictionary<int, string> _secondaryPolutantTypes;

        
        public AzoHrMetaData()
        {
            InitStations();
            InitDataTypes();
            InitPolutantTypes();
            InitHourlyPolutantTypes();
            InitSecondaryPolutantTypes();
            InitEightHoursPolutantTypes();
        }

        private void InitDataTypes()
        {
            _dataTypes = new Dictionary<int, string>
            {
                {(int)AzoHrDataType.SatniIzvorni, "Satni izvorni"},
                {(int)AzoHrDataType.OsamSatniIzvorni, "Osmosatni izvorni"},
                {(int)AzoHrDataType.DnevniIzvorni, "Dnevni Izvorni"}
            };
        }

        private void InitHourlyPolutantTypes()
        {
            _hourlyPolutantTypes = new[] {
                (int)AzoHrPolutant.NO2,
                (int)AzoHrPolutant.SO2,
                (int)AzoHrPolutant.CO,
                (int)AzoHrPolutant.Pm10,
                (int)AzoHrPolutant.O3,
                (int)AzoHrPolutant.H2S,
                (int)AzoHrPolutant.Pm2Point5,
                (int)AzoHrPolutant.Benzene,
                (int)AzoHrPolutant.Methane,
                (int)AzoHrPolutant.NH3
            };
        }


        private void InitEightHoursPolutantTypes()
        {
            _eightHoursPolutantTypes = new[] { (int)AzoHrPolutant.CO, (int)AzoHrPolutant.O3 };
        }


        private void InitPolutantTypes()
        {
            _polutantTypes = new Dictionary<int,string>
            {
                {1, "NO₂ - dušikov dioksid (µg/m3)"},
                {2, "SO₂ - sumporov dioksid (µg/m3)"},
                {3, "CO - ugljikov monoksid (mg/m3)"},
                {5, "PM₁₀ - lebdeće čestice (&lt;10µm) (µg/m3)"},
                {31, "O₃ - ozon (µg/m3)"},
                {4, "H₂S - sumporovodik (µg/m3)"},
                {28, "PM₂.₅ - lebdeće čestice (&lt;2.5µm) (µg/m3)"},
                {32, "benzen (µg/m3)"},
                {71, "metan (µg/m3)"},
                {95, "NH₃ - amonijak (µg/m3)"},

                {38, "NOₓ izraženi kao NO₂ - dušikovi oksidi (µg/m3)"},
                {49, "butadien 1,3 (µg/m3)"},
                {60, "toluen (µg/m3)"},
                {61, "etil benzen (µg/m3)"},
                {244, "Hg0 + Hg-reactive - ukupna plinovita živa (ng/m3)"},
                {309, "Al - aluminij (ng/m3)"}
            };
        }

        private void InitSecondaryPolutantTypes()
        {
            _secondaryPolutantTypes = new Dictionary<int, string>
            {
                {30, "Pb u PM₁₀ - olovo u PM10 (µg/m3)"},
                {33, "Cd u PM₁₀ - kadmij u PM10 (ng/m3)"},
                {34, "As u PM₁₀ - arsen u PM10 (ng/m3)"},
                {35, "Ni u PM₁₀ - nikal u PM10 (ng/m3)"},
                {80, "BaP u PM₁₀ - Benzo(a)piren u PM10 (ng/m3)"},
                {100, "Cr u PM₂.₅ - krom u PM2.5 (ng/m3)"},
                {101, "Mn u PM₂.₅ - mangan u PM2.5 (ng/m3)"},
                {104, "Amonij u PM₂.₅ (µg/m3)"},
                {105, "Nitrati u PM2.5 (µg/m3)"},
                {106, "Sulfati  u PM2.5 (µg/m3)"},
                {108, "V u PM₂.₅  - vanadij  u PM2.5 (ng/m3)"},
                {109, "Zn u PM₂.₅ - cink u PM2.5 (ng/m3)"},
                {110, "Co u PM₂.₅ - kobalt u PM2.5 (ng/m3)"},
                {112, "Cu u PM₂.₅ - bakar u PM2.5 (ng/m3)"},
                {120, "Ca u PM₂.₅ - kalcij u PM2.5 (µg/m3)"},
                {121, "kloridi u PM2.5 (µg/m3)"},
                {123, "K u PM₂.₅ - kalij u PM2.5 (µg/m3)"},
                {124, "Mg u PM₂.₅ - magnezij u PM2.5 (µg/m3)"},
                {125, "Na u PM₂.₅ - natrij u PM2.5 (µg/m3)"},
                {128, "EC u PM₂.₅  - elementarni ugljik u PM2.5 (µg/m3)"},
                {129, "OC u PM₂.₅ - organski ugljik  u PM2.5 (µg/m3)"},
                {256, "Mn u PM₁₀ - mangan u PM10 (aerosol) (ng/m3)"},
                {266, "Zn u PM₁₀ - cink u PM10 (aerosol) (ng/m3)"},
                {268, "Fe u PM₁₀ - željezo u PM10 (aerosol) (ng/m3)"},
                {269, "Cu u PM₁₀ - bakar u PM10 (aerosol) (ng/m3)"},
                {272, "BaP u PM₁₀ - Benzo(a)piren u PM10 (air+aerosol) (ng/m3)"},
                {280, "Dibenzo(a,h)antracen u PM10 (aerosol) (ng/m3)"},
                {283, "Benzo(a)antracen u PM10"},
                {285, "Benzo(b)fluoranten u PM10"},
                {287, "Benzo(k)fluoranten u PM10"},
                {291, "Indeno(1,2,3,-cd)piren u PM10"},
                {475, "temperatura (℃)"},
                {477, "brzina vjetra (m/s)"},
                {478, "smjer vjetra (°)"},
                {479, "relativna vlažnost (%)"}
            };

        }

        private void InitStations()
        {
            _azoHrStations = new Dictionary<int, AzoHrStation>();
            _rawData.Replace("\r\n", "\n").Split('\n')
                .Each(row =>
                {
                    var cols = row.Split('\t');
                    _azoHrStations.Add(int.Parse(cols[4]), new AzoHrStation(cols[1], cols[0],
                        double.Parse(cols[3], new CultureInfo("hr-HR")), double.Parse(cols[2], new CultureInfo("hr-HR")),
                        int.Parse(cols[4])));
                });
        }

        public Dictionary<int, AzoHrStation> GetStations()
        {
            return _azoHrStations;
        }

        public Dictionary<int, string> GetDataTypes()
        {
            return _dataTypes;
        }

        public Dictionary<int, string> GetPolutantTypes()
        {
            return _polutantTypes;
        }

        public Dictionary<int, string> GetSecondaryPolutantTypes()
        {
            return _secondaryPolutantTypes;
        }

        public int[] GetHourlyPolutantTypes()
        {
            return _hourlyPolutantTypes;
        }

        public int[] GetEightHoursPolutantTypes()
        {
            return _eightHoursPolutantTypes;
        }

        public int[] GetDailyPolutantTypes()
        {
            return _hourlyPolutantTypes;
        }

        public string RunQueryDiagnostics(string resultFilePath, int timeoutBetweenQueries)
        {
            HttpClient client = new HttpClient();
            using (var fileStream = File.CreateText(resultFilePath))
            {
                fileStream.WriteLineAsync($"Started at {DateTime.Now.ToString(CultureInfo.InvariantCulture)}"); 
             GetStations().Each(station =>
                    GetDataTypes().Each(dataType =>
                        GetPolutantTypes().Union(GetSecondaryPolutantTypes()).Each(polutantType =>
                            {
                                var line =$"{station.Key}\t{station.Value.Name}\t{polutantType.Key}\t{polutantType.Value}\t{dataType.Key}\t{dataType.Value}";
                                try
                                {
                                    
                                    var result =
                                        client.GetAsync(
                                                $"http://iszz.azo.hr/iskzl/rs/podatak/export/json?postaja={station.Key}&polutant={polutantType.Key}&tipPodatka={dataType.Key}&vrijemeOd=14.10.2016&vrijemeDo=14.10.2016")
                                            .Result.Content.ReadAsStringAsync().Result;

                                    if (string.IsNullOrEmpty(result))
                                    {
                                        line += $"\tEmpty\t";
                                    }
                                    else if (result.Length < 10)
                                    {
                                        line += $"\tShort\t{result}";
                                    }
                                    else
                                    {
                                        line += $"\tOK\t{(result.Length > 100 ? result.Substring(0,100) : result)}";
                                    }
                                    
                                }
                                catch (Exception ex)
                                {
                                    line += $"\t{ex.Message}";
                                }
                                finally
                                {
                                    fileStream.WriteLineAsync(line).Wait();
                                    Task.Delay(timeoutBetweenQueries).Wait();
                                }
                            }
                )));  
            }

            return Path.GetFullPath(resultFilePath);
        }
    }

}
