namespace Smog.Infrastructure.DataProviders.Azohr
{
    public class AzoHrStation
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
        public int QueryId { get; set; }

        public AzoHrStation(string id, string name, double lon, double lat, int queryId)
        {
            Id = id;
            Name = name;
            Lon = lon;
            Lat = lat;
            QueryId = queryId;
        }
    }
}