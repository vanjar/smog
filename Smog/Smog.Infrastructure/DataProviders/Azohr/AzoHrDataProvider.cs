﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using Smog.Infrastructure.Extensions;
using Smog.Infrastructure.Model;
using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.DataProviders.Azohr
{
    public class AzoHrDataProvider : ISmogDataProvider, ITestableSmogDataProvider
    {
        private readonly Dictionary<int, SmogPolutantInterval> _intervalsByAzoDataType = new Dictionary<int, SmogPolutantInterval>
        {
            {(int)AzoHrDataType.SatniIzvorni,SmogPolutantInterval.Hour },
            {(int)AzoHrDataType.OsamSatniIzvorni,SmogPolutantInterval.EightHour },
            {(int)AzoHrDataType.DnevniIzvorni,SmogPolutantInterval.TwentyFourHour },
        };

        readonly Dictionary<int, SmogPolutantType> _polutantTypeByAzoPolutant = new Dictionary<int, SmogPolutantType>
        {
            {(int)AzoHrPolutant.NO2,SmogPolutantType.NO2},
            {(int)AzoHrPolutant.SO2,SmogPolutantType.SO2},
            {(int)AzoHrPolutant.CO,SmogPolutantType.CO},
            {(int)AzoHrPolutant.Pm10,SmogPolutantType.Pm10},
            {(int)AzoHrPolutant.O3,SmogPolutantType.O3},
            {(int)AzoHrPolutant.H2S,SmogPolutantType.H2S},
            {(int)AzoHrPolutant.Pm2Point5,SmogPolutantType.Pm2Point5},
            {(int)AzoHrPolutant.Benzene,SmogPolutantType.Benzene},
            {(int)AzoHrPolutant.Methane,SmogPolutantType.Methane},
            {(int)AzoHrPolutant.NH3,SmogPolutantType.NH3},
        };

        private readonly AzoHrMetaData _azoHrMetaData;
        private readonly HttpClient _httpClient;

        public string ProviderName => "Agencija za zaštitu okoliša";
        public string ProviderId => DataProvidersIds.AzoHr;
        public int IntervalInMinutes => 60;
        public TimeZoneInfo TimeZoneInfo => TimeZoneInfo.GetSystemTimeZones()[49];
        public string Country => "hr-HR";

        public int NumberOfAllowedRequests { get; set; }
        public int DelayInMilisecondsBetweenRequests { get; set; }

        public AzoHrDataProvider(AzoHrMetaData azoHrMetaData)
        {
            _azoHrMetaData = azoHrMetaData;
            _httpClient = new HttpClient();
            NumberOfAllowedRequests = -1;
            DelayInMilisecondsBetweenRequests = 500;
        }


        public async Task<IEnumerable<SmogDto>> GetDataAsync()
        {
            var functionName = $"{this.GetType().Name}.GetDataAsync()";
            Log.Debug($"{functionName} started.");
            List<SmogDto> smogDtos = new List<SmogDto>();

            int allowedRequestCounter = 0;

            foreach (var station in _azoHrMetaData.GetStations())
            {
                Dictionary<DateTimeOffset, Dictionary<string, double>> polutantResult
                  = new Dictionary<DateTimeOffset, Dictionary<string, double>>();
                var fetchDate = DateTime.Now;
                foreach (var dataType in _azoHrMetaData.GetDataTypes())
                {

                    foreach (var polutant in _azoHrMetaData.GetHourlyPolutantTypes())
                    {
                        if (allowedRequestCounter >= NumberOfAllowedRequests && NumberOfAllowedRequests > 0)
                            break;

                        if (dataType.Key == (int)AzoHrDataType.OsamSatniIzvorni &&
                            !_azoHrMetaData.GetEightHoursPolutantTypes().Any(p => p == polutant))
                        {
                            Log.Debug($"{functionName} skipping get data for station:{station.Value.Name},dataType: {dataType.Value}, polutant: {_azoHrMetaData.GetPolutantTypes()[polutant]} started.");
                            break;
                        }
                        allowedRequestCounter++;
                        string url = "";
                        try
                        {
                            Log.Debug(
                                $"{functionName} get data for station:{station.Value.Name},dataType: {dataType.Value}, polutant: {_azoHrMetaData.GetPolutantTypes()[polutant]} started.");

                            url = GetFetchUrl(station.Key, dataType.Key, polutant);
                            var httpResponse = await _httpClient.GetAsync(url, CancellationToken.None);
                            httpResponse.EnsureSuccessStatusCode();
                            var responseContent = await httpResponse.Content.ReadAsStringAsync();
                            var azoHrDtos = JsonConvert.DeserializeObject<AzoHrDto[]>(responseContent,
                                new JsonSerializerSettings
                                {
                                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                                });
                            var azoHrDtoResultCount = azoHrDtos?.Count() ?? 0;
                            Log.Debug(
                                $"{functionName} get data for station: {station.Value.Name},dataType: {dataType.Value}, polutant: {_azoHrMetaData.GetPolutantTypes()[polutant]} finished with count: {azoHrDtoResultCount}.");
                            if (azoHrDtoResultCount > 0)
                            {
                                azoHrDtos.Each(x =>
                                {
                                    if (!polutantResult.ContainsKey(x.Podatak.Vrijeme))
                                        polutantResult.Add(x.Podatak.Vrijeme, new Dictionary<string, double>());

                                    var polutantKey = $"{polutant};{dataType.Key}";
                                    if (!polutantResult[x.Podatak.Vrijeme].ContainsKey(polutantKey))
                                        polutantResult[x.Podatak.Vrijeme].Add(polutantKey, (double)x.Podatak.Vrijednost);
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex,
                                $"{functionName} error, url: {url}, station: {station.Value.Name},dataType: {dataType.Value}, polutant: {_azoHrMetaData.GetPolutantTypes()[polutant]}.");
                        }
                        await Task.Delay(DelayInMilisecondsBetweenRequests, CancellationToken.None);
                    }
                }
                smogDtos.AddRange(polutantResult
                      .Select(p => new SmogDto(fetchDate, null, station.Value.Name, station.Value.Id,
                                  station.Value.Lon,
                                  station.Value.Lat, 0, this.Country, this.ProviderId, null,
                                  TimeZoneInfo.ConvertTime(p.Key, TimeZoneInfo),
                                  TimeZoneInfo.ConvertTime(p.Key, TimeZoneInfo), TimeZoneInfo.Id, ParseMeasurementValues(p.Value))));
            }
            Log.Debug($"{functionName} finished with stations: {string.Join(",", smogDtos.Select(x => x.StationName).Distinct())}");
            return smogDtos;
        }



        private SmogPolutantDto[] ParseMeasurementValues(Dictionary<string, double> measurementValues)
        {
            return measurementValues.Select(mv => new
            {
                interval = _intervalsByAzoDataType[int.Parse(mv.Key.Split(';')[1])],
                polutant = _polutantTypeByAzoPolutant[int.Parse(mv.Key.Split(';')[0])],
                value = mv.Value
            }).Select(pmv => new SmogPolutantDto(pmv.value, pmv.polutant, pmv.interval, GetUnitByType(pmv.polutant)))
             .ToArray();
        }

        private SmogPolutantUnit GetUnitByType(SmogPolutantType type)
        {
            if (type == SmogPolutantType.CO)
                return SmogPolutantUnit.MiliGramsPerCubicMetre;
            return SmogPolutantUnit.MikroGramsPerCubicMetre;
        }
    
        private string GetFetchUrl(int station, int dataType, int polutant)
        {
            var datePattern = DateTime.Now.Date.ToString("dd.MM.yyyy");
            return $"http://iszz.azo.hr/iskzl/rs/podatak/export/json?" +
                   $"postaja={station}&polutant={polutant}" +
                   $"&tipPodatka={dataType}" +
                   $"&vrijemeOd={datePattern}&vrijemeDo={datePattern}";
        }
    }
}
