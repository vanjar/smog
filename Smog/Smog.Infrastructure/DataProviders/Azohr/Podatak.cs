﻿using System;

namespace Smog.Infrastructure.DataProviders.Azohr
{
    public class Podatak
    {
        public double Vrijednost { get; set; }
        public string MjernaJedinica { get; set; }
        public DateTimeOffset Vrijeme { get; set; }
    }

    public class AzoHrDto
    {
        public Podatak Podatak { get; set; }
    }
    
}
