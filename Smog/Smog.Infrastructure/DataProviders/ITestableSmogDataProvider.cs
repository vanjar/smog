namespace Smog.Infrastructure.DataProviders
{
    public interface ITestableSmogDataProvider
    {
        int NumberOfAllowedRequests { get; set; }
        int DelayInMilisecondsBetweenRequests { get; set; }
    }
}