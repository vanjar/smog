﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Smog.Infrastructure.Model;

namespace Smog.Infrastructure.DataProviders
{
    public interface ISmogDataProvider
    {
        Task<IEnumerable<SmogDto>> GetDataAsync();

        string ProviderName { get; }

        string ProviderId { get;  }

        int IntervalInMinutes { get; }

        TimeZoneInfo TimeZoneInfo{ get; }

        string Country { get; }
        
    }
}