﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Marten;
using Marten.Linq;
using Serilog;
using Smog.Infrastructure.Model;

namespace Smog.Infrastructure.DataAccess
{
    public class SmogDatabase : ISmogDatabase
    {
        private readonly IDocumentStore _store;

        public SmogDatabase(IDocumentStore store)
        {
            _store = store;
        }
        
        public async Task InsertSmogDataAsync(SmogDto smogDto)
        {
            using (var session = _store.LightweightSession())
            {
              var existing = await session.Query<SmogDto>().SingleOrDefaultAsync(x => x.StationId == smogDto.StationId
                && x.IntervalStart == smogDto.IntervalStart.KeepOnlyHoursAndMinutes()).ConfigureAwait(false);
                if (existing != null)
                    session.Delete<SmogDto>(existing.SmogDataId);
                session.Store(smogDto);
                await session.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public async Task<SmogDto> GetSmogDataByIdAsync(string smogDataId)
        {
            using (var session = _store.LightweightSession())
            {
                return await session.LoadAsync<SmogDto>(smogDataId).ConfigureAwait(false);
            }
    }

        public async Task<IEnumerable<SmogDto>> GetSmogDataDetailsAsync(string stationKey, DateTimeOffset current)
        {
            var from = current.AddDays(-120);
            using (var session = _store.LightweightSession())
            {
                return await session.Query<SmogDto>()
                    .Where(x => x.Key == stationKey)
                    .Where(x => x.IntervalStart >= from && x.IntervalStart <= current)
                    .ToListAsync().ConfigureAwait(false);
            }
        }

        public async Task<IEnumerable<SmogDto>> GetSmogDataByProviderIdAsync(string providerId)
        {
            using (var session = _store.LightweightSession())
            {
                return await session.Query<SmogDto>()
                    .Where(x => x.ProviderId == providerId).ToListAsync().ConfigureAwait(false);
            }
        }

        public async Task<IEnumerable<SmogDto>> GetSmogDataByIntervalAsync(DateTimeOffset intervalStart, DateTimeOffset intervalEnd)
        {
            intervalStart = intervalStart.KeepOnlyHoursAndMinutes();
            intervalEnd = intervalEnd.KeepOnlyHoursAndMinutes();
            using (var session = _store.LightweightSession())
            {
                return await session.Query<SmogDto>()
                    .Where(x => x.IntervalStart>=intervalStart && x.IntervalEnd<= intervalEnd)
                    .ToListAsync().ConfigureAwait(false);
            }
        }

        public async Task<SmogDto> GetLatestSmogDataForStationAsync(string stationId)
        {
            var dtos =  await GetLatestSmogDataByStationAsync(stationId).ConfigureAwait(false);
            return dtos.SingleOrDefault();
        }

        public async Task<Dictionary<string,List<SmogDto>>>  GetLatestPast24HourSmogDataAsync()
        {
            return await GetLatestPast24HourSmogDataAsync(null).ConfigureAwait(false);
        }

        public async Task<Dictionary<string, List<SmogDto>>> GetLatestPast24HourSmogDataAsync(string stationKey)
        {
            using (var session = _store.LightweightSession())
            {
                var result = new Dictionary<string, List<SmogDto>>();

                var stationKeys = stationKey == null
                    ? (await session.Query<SmogDto>()
                        .Select(x => new { x.StationId,x.ProviderId}).Distinct()
                        .ToListAsync()
                        .ConfigureAwait(false))
                        .Select(x=>StationKey.Create(x.StationId, x.ProviderId))
                    : new List<string> { stationKey };

                foreach (var key in stationKeys)
                {
                    IList<SmogDto> latestPast24HourData = await session.Query<SmogDto>()
                       .Where(x => x.StationId == StationKey.GetStationId(key) && x.ProviderId == StationKey.GetProviderId(key))
                       .OrderByDescending(x => x.IntervalStart)
                       .Take(24)
                       .ToListAsync().ConfigureAwait(false);

                    var latest = latestPast24HourData.OrderByDescending(x => x.IntervalStart).FirstOrDefault();
                    if (latest == null)
                        continue;

                    result.Add(key, latestPast24HourData.Where(x => x.IntervalStart >= latest.IntervalStart.AddHours(-24)).ToList());

                }
                return result;
            }
        }
        
        public async Task<IEnumerable<SmogDto>> GetLatestSmogDataByStationAsync()
        {
            return await GetLatestSmogDataByStationAsync(null).ConfigureAwait(false);
        }

        private async Task<IEnumerable<SmogDto>> GetLatestSmogDataByStationAsync(string stationId)
        {
            using (var session = _store.LightweightSession())
            {
                var stationIds = stationId == null
                    ? await session.Query<SmogDto>()
                        .Select(x => x.StationId).Distinct()
                        .ToListAsync()
                        .ConfigureAwait(false)
                    : new List<string> { stationId };


                List<SmogDto> smogDtos = new List<SmogDto>();
                foreach (var id in stationIds)
                {
                    var latest = await session.Query<SmogDto>()
                        .Where(x => x.StationId == id)
                        .OrderByDescending(x => x.IntervalStart)
                        .FirstAsync();
                    smogDtos.Add(latest);
                }

                return smogDtos;
            }
        }
        

        public async Task SaveDataProviderInfoAsync(DataProviderInfoDto dto)
        {
            using (var session = _store.LightweightSession())
            {
                session.Store<DataProviderInfoDto>(dto);
                await session.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public async Task<IEnumerable<DataProviderInfoDto>> GetDataProviderInfosAsync()
        {
            using (var session = _store.LightweightSession())
            {
                var result = await session.Query<DataProviderInfoDto>().ToListAsync().ConfigureAwait(false);
                return result;
            }
        }

        public async Task<DataProviderInfoDto> GetLatestSuccessfulDataProviderInfoAsync(string dataProviderId)
        {
            using (var session = _store.LightweightSession())
            {
                var latest = await session.Query<DataProviderInfoDto>()
                    .Where(d => d.ProviderId == dataProviderId)
                    .Where(d => d.Status == LastFetchStatus.Ok || d.Status == LastFetchStatus.Warning)
                    .OrderByDescending(dto => dto.LastFetchStart)
                    .FirstOrDefaultAsync().ConfigureAwait(false);
                return latest;
            }
        }


        public async Task SaveStationGeoAsync(StationGeoDto dto)
        {
            using (var session = _store.LightweightSession())
            {
                session.Store<StationGeoDto>(dto);
                await session.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public async Task<IEnumerable<StationGeoDto>>  GetStationGeosAsync()
        {
            using (var session = _store.LightweightSession())
            {
                var stations = await session.Query<StationGeoDto>().ToListAsync()
                    .ConfigureAwait(false);
                return stations;
            }
        }

        public async Task<IEnumerable<StationGeoDto>> GetDistinctSmogDataStationsWithLocation()
        {
            using (var session = _store.LightweightSession())
            {
                var stationGeos = await session.Query<SmogDto>()
                    .Select(x=> new { x.StationId,x.ProviderId, x.Longitude, x.Latitude})
                    .Distinct()
                    .ToListAsync()
                    .ConfigureAwait(false);

               return stationGeos.Select(
                    x => new StationGeoDto {StationId = x.StationId,ProviderId = x.ProviderId,Longitude = x.Longitude, Latitude = x.Latitude});
            }
        }
    }
}
