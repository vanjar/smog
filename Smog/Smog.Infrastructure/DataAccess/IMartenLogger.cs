﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marten;
using Marten.Services;
using Npgsql;
using Serilog;

namespace Smog.Infrastructure.DataAccess
{

    public class MartenLogger : IMartenLogger, IMartenSessionLogger
    {
        public IMartenSessionLogger StartSession(IQuerySession session)
        {
            return this;
        }

        public void SchemaChange(string sql)
        {
            Log.ForContext<MartenLogger>().Debug($"Executing DDL change: {sql}");
        }

        public void LogSuccess(NpgsqlCommand command)
        {
           // Console.WriteLine(command.CommandText);
        }

        public void LogFailure(NpgsqlCommand command, Exception ex)
        {
            Log.ForContext<MartenLogger>().Error(ex,$"Postgresql command failed: {command.CommandText}");
        }

        public void RecordSavedChanges(IDocumentSession session, IChangeSet commit)
        {
            var lastCommit = commit;
           Log.ForContext<MartenLogger>().Debug($"Persisted {lastCommit.Updated.Count()} updates, {lastCommit.Inserted.Count()} inserts, and {lastCommit.Deleted.Count()} deletions");
        }
    }
}
