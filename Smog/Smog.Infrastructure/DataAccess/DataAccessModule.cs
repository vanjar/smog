﻿using Autofac;
using Marten;

namespace Smog.Infrastructure.DataAccess
{
    public class DataAccessModule :  Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance<DocumentStore>(DocumentStore.For(ds =>
            {
                ds.Logger(new ConsoleMartenLogger());
                ds.AutoCreateSchemaObjects = AutoCreate.All;
                ds.Connection("host=localhost;port=5432;database=smog;password=password1#;username=postgres");
                ds.Schema.Include<SmogMartenRegistry>();
            })).As<IDocumentStore>().SingleInstance();
            builder.RegisterType<SmogDatabase>().As<ISmogDatabase>().SingleInstance();
        }
    }
}
