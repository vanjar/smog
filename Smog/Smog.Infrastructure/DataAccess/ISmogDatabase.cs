using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Smog.Infrastructure.Model;

namespace Smog.Infrastructure.DataAccess
{
    public interface ISmogDatabase
    {
        Task InsertSmogDataAsync(SmogDto smogDto);
        Task<SmogDto> GetSmogDataByIdAsync(string smogDataId);
        Task<IEnumerable<SmogDto>> GetSmogDataDetailsAsync(string stationKey, DateTimeOffset current);
        Task<IEnumerable<SmogDto>> GetSmogDataByProviderIdAsync(string providerId);
        Task<IEnumerable<SmogDto>> GetSmogDataByIntervalAsync(DateTimeOffset intervalStart, DateTimeOffset intervalEnd);
        Task<SmogDto> GetLatestSmogDataForStationAsync(string stationId);
        Task<IEnumerable<SmogDto>> GetLatestSmogDataByStationAsync();
        Task<Dictionary<string,List<SmogDto>>> GetLatestPast24HourSmogDataAsync(string stationId);
        Task<Dictionary<string, List<SmogDto>>> GetLatestPast24HourSmogDataAsync();
        Task SaveDataProviderInfoAsync(DataProviderInfoDto dto);
        Task SaveStationGeoAsync(StationGeoDto dto);
        Task<IEnumerable<StationGeoDto>> GetDistinctSmogDataStationsWithLocation();
        Task<IEnumerable<StationGeoDto>> GetStationGeosAsync();
        Task<IEnumerable<DataProviderInfoDto>> GetDataProviderInfosAsync();
        Task<DataProviderInfoDto> GetLatestSuccessfulDataProviderInfoAsync(string dataProviderId);


    }
}