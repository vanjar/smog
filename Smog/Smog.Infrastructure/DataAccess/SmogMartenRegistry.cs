﻿using Marten;
using Smog.Infrastructure.Model;

namespace Smog.Infrastructure.DataAccess
{
    public class SmogMartenRegistry : MartenRegistry
    {
        public SmogMartenRegistry()
        {
            For<SmogDto>().GinIndexJsonData()
                //.Duplicate(x => x.Latitude)
                //.Duplicate(x => x.Longitude)
                //.Duplicate(x => x.IntervalStart.UtcDateTime,pgType:"timestamp")
                //.Duplicate(x => x.IntervalEnd)
                .Index(x => x.StationId)
                .Index(x => x.ProviderId);

            For<DataProviderInfoDto>().GinIndexJsonData()
                .Index(x => x.ProviderId);

            For<StationGeoDto>().Index(i=> i.StationId, i=> i.IsUnique = true).GinIndexJsonData();

        }
    }
}
