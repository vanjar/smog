﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Serilog;
using Serilog.Exceptions;
using Smog.Infrastructure.Cache;
using Smog.Infrastructure.DataAccess;
using Smog.Infrastructure.DataProviders;
using Smog.Infrastructure.DataProviders.Arso;
using Smog.Infrastructure.DataProviders.Azohr;
using Smog.Infrastructure.GeoCodingApi;
using Smog.Infrastructure.Logging;
using Smog.Infrastructure.Model;
using Smog.Infrastructure.Services;
using Smog.Infrastructure.Services.Aqi;
using Smog.Infrastructure.Services.Aqi.UsEpaAqi;

namespace Smog.Infrastructure
{
    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SmogDatabase>().SingleInstance();

            builder.RegisterType<ArsoDataProvider>().As<ISmogDataProvider>().AsSelf();
            builder.RegisterType<ArsoResponseRegexParser>();

            builder.RegisterType<AzoHrMetaData>().SingleInstance();
            builder.RegisterType<AzoHrDataProvider>().As<ISmogDataProvider>().AsSelf();

            builder.RegisterType<ResponseHasher>().SingleInstance();

            builder.RegisterType<SmogDataService<ArsoDataProvider>>().As<ISmogDataService>().AsSelf();
            builder.RegisterType<SmogDataService<AzoHrDataProvider>>().As<ISmogDataService>().AsSelf();
            builder.RegisterType<SmogCache>().SingleInstance();
            builder.RegisterType<SmogCacheService>().As<IService>().AsSelf();
            builder.RegisterType<StationGeoDataService>().As<IService>().AsSelf();

            builder.RegisterType<UsEpaAqiCalculator>().As<IAqiCalculator>().SingleInstance();
            builder.RegisterType<GeoCodingApiClient>().SingleInstance();
            InitLogger();
    
        }

        private void InitLogger()
        {
            Log.Logger = new LoggerConfiguration()
             .MinimumLevel.Debug()
             .Enrich.FromLogContext()
             .Enrich.WithExceptionDetails()
             .Enrich.With(new ThreadEnricher())
             .WriteTo.RollingFile(AppContext.BaseDirectory + @"\Logs\SmogLog-{Date}.txt", outputTemplate:
                    "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz}\t[{Level}]\t{SourceContext}\t({ThreadId})\t{Message}{NewLine}{Exception}")
              .WriteTo.Seq("http://localhost:5341")
             .CreateLogger();
        }
    }
}
