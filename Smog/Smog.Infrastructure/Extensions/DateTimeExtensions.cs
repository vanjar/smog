﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smog.Infrastructure
{
    public static class DateTimeExtensions
    {
        public static DateTimeOffset KeepOnlyHoursAndMinutes(this DateTimeOffset dt)
        {
            return new DateTimeOffset(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, 0,dt.Offset);
        }
    }
}
