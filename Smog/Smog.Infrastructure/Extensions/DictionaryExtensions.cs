﻿using System;
using System.Collections.Generic;

namespace Smog.Infrastructure.Extensions
{
    public static class DictionaryExtensions
    {
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue value;
            dictionary.TryGetValue(key, out value);
            return value;
        }

        public static TValue? GetValueOrNull<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)  where TValue : struct
        {
            TValue value;
            var exists = dictionary.TryGetValue(key, out value);
            if (exists)
                return value;
            return null;
        }

        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TValue> create)
        {
            TValue value;
            if (!dictionary.TryGetValue(key, out value))
            {
                value = create(key);
                dictionary.Add(key, value);
            }
            return value;
        }
    }
}
