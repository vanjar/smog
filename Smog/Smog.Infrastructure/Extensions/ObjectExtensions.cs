﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Smog.Infrastructure
{
    public static class ObjectExtensions
    {
    
        public static bool IsAnyOf<T>(this T value, params T[] values)
        {
            return value.IsAnyOf(EqualityComparer<T>.Default, values);
        }

        public static bool IsAnyOf<T>(this T value, IEnumerable<T> values)
        {
            return value.IsAnyOf(EqualityComparer<T>.Default, values);
        }

        public static bool IsAnyOf<T>(this T value, IEqualityComparer<T> comparer, params T[] values)
        {
            return value.IsAnyOf(comparer, (IEnumerable<T>)values);
        }

        public static bool IsAnyOf<T>(this T value, IEqualityComparer<T> comparer, IEnumerable<T> values)
        {
            return values.Any(arrayValue => comparer.Equals(value, arrayValue));
        }

        public static string ToJson(this object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        public static bool IsNull(this object value)
        {
            return value == null || value == DBNull.Value;
        }

        public static byte AsByte(this object value)
        {
            return value is byte ? (byte)value : Convert.ToByte(value);
        }

        public static byte? AsNullableByte(this object value)
        {
            return value.IsNull() ? (byte?)null : value.AsByte();
        }

        public static int AsInt32(this object value)
        {
            return value is int ? (int)value : Convert.ToInt32(value);
        }

        public static int? AsNullableInt32(this object value)
        {
            return value.IsNull() ? (int?)null : value.AsInt32();
        }

        public static long AsInt64(this object value)
        {
            return value is long ? (long)value : Convert.ToInt64(value);
        }

        public static long? AsNullableInt64(this object value)
        {
            return value.IsNull() ? (long?)null : value.AsInt64();
        }

        public static double AsDouble(this object value)
        {
            return value is double ? (double)value : Convert.ToDouble(value);
        }

        public static double? AsNullableDouble(this object value)
        {
            return value.IsNull() ? (double?)null : value.AsDouble();
        }

        public static DateTime AsDateTime(this object value)
        {
            return value is DateTime ? (DateTime)value : Convert.ToDateTime(value);
        }

        public static DateTime? AsNullableDateTime(this object value)
        {
            return value.IsNull() ? (DateTime?)null : value.AsDateTime();
        }

        public static DateTime AsUtcDateTime(this object value)
        {

            return DateTime.SpecifyKind(value is DateTime ? (DateTime)value : Convert.ToDateTime(value), DateTimeKind.Utc);
        }

        public static DateTime? AsNullableUtcDateTime(this object value)
        {
            return value.IsNull() ? (DateTime?)null : value.AsUtcDateTime();
        }

        public static bool AsBoolean(this object value)
        {
            return value is bool ? (bool)value : Convert.ToBoolean(value);
        }

        public static bool? AsNullableBoolean(this object value)
        {
            return value.IsNull() ? (bool?)null : value.AsBoolean();
        }

        public static string AsString(this object value, IFormatProvider formatProvider = null)
        {
            if (value.IsNull())
                return null;
            return value is string ? (string)value : Convert.ToString(value, formatProvider);
        }

        public static byte[] AsByteArray(this object value)
        {
            return value.IsNull() ? null : (byte[])value;
        }

       
    }
}
