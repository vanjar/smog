﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smog.Infrastructure
{

    public static class EnumerableExtensions
    {
        public static void Each<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            if (action == null)
                throw new ArgumentNullException("action");
            if (source != null)
            {
                foreach (TSource item in source)
                {
                    action(item);
                }
            }
        }

        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(
         this IEnumerable<TSource> source,
         int batchSize)
        {
            var batch = new List<TSource>();
            foreach (var item in source)
            {
                batch.Add(item);
                if (batch.Count == batchSize)
                {
                    yield return batch;
                    batch = new List<TSource>();
                }
            }

            if (batch.Any()) yield return batch;
        }
    }
}
