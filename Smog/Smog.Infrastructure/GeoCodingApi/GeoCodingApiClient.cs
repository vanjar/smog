﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using Smog.Infrastructure.DataProviders.Azohr;

namespace Smog.Infrastructure.GeoCodingApi
{
 

    public class GeoCodingApiClient
    {
        private const string API_KEY = "AIzaSyCOgsw9vIvhiPsJzxNQZns4Ho1qguD3ezc";

        private HttpClient _httpClient;

        public GeoCodingApiClient()
        {
            _httpClient = new HttpClient();
        }
        
        public async Task<GoogleReverseGeoCodingResponse> GetAsync(double lat, double lng)
        {
            Log.Logger.Information($"Get geo api data from: {GetApiRequestLink(lat, lng)}.");
            var url = GetApiRequestLink(lat, lng);
            var httpResponse =  await _httpClient.GetAsync(url);
            httpResponse.EnsureSuccessStatusCode();
            var responseContent = await httpResponse.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<GoogleReverseGeoCodingResponse>(responseContent,
                     new JsonSerializerSettings
                     {
                         ContractResolver = new CamelCasePropertyNamesContractResolver()
                     });
            return result;
        }
        private string GetApiRequestLink(double lat, double lng)
        {
            return $"https://maps.googleapis.com/maps/api/geocode/json?latlng={lat.ToString(CultureInfo.InvariantCulture)},{lng.ToString(CultureInfo.InvariantCulture)}&key={API_KEY}";
        }
    }
}
