﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Smog.Infrastructure.DataAccess;
using Smog.Infrastructure.GeoCodingApi;
using Smog.Infrastructure.Model;

namespace Smog.Infrastructure.Services
{
    public class StationGeoDataService : IService
    {
        private readonly SmogDatabase _smogDatabase;
        private readonly GeoCodingApiClient _geoApiClient;
        private Timer _timer;
        private bool _isRunning;
        private readonly object _syncRoot = new object();

        public StationGeoDataService(SmogDatabase smogDatabase, GeoCodingApiClient geoApiClient)
        {
            _smogDatabase = smogDatabase;
            _geoApiClient = geoApiClient;
            _timer = new Timer(OnTimerElapsed, null, Timeout.Infinite, 0);
        }


        public async Task<ServiceStartResult> StartAsync(CancellationToken cancellationToken)
        {
            Log.Information($"Starting StationGeoDataService.");
            _timer.Change(0,(int)TimeSpan.FromHours(24).TotalMilliseconds);
            return ServiceStartResult.Continue;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Log.Information($"Stopping StationGeoDataService.");
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }
            return Task.FromResult(true);
        }

        private async void OnTimerElapsed(object state)
        {
            lock (_syncRoot)
            {
                if (_timer == null || _isRunning)
                    return;

                _isRunning = true;
            }
         
            try
            {
                Log.Logger.Information($"StationGeoDataService.OnTimerElapsed() start.");
                var timestamp = DateTimeOffset.Now;
                var geoDataFromDtos = (await _smogDatabase.GetDistinctSmogDataStationsWithLocation()).ToArray();
                Log.Logger.Information($"Found {geoDataFromDtos.Length} existing geo data from smog dto.");
                var existingGeos = (await _smogDatabase.GetStationGeosAsync()).ToArray();
                Log.Logger.Information($"Found {existingGeos.Length} existing geo data.");
                var nonexistent = geoDataFromDtos.Where(d=> !existingGeos.Any(e=>e.Key == d.Key
                    && e.Longitude == d.Longitude && e.Latitude == d.Latitude)).ToArray();
                Log.Logger.Information($"Found {nonexistent.Length} nonexistent geo data.");
                var old = existingGeos.Where(x=>x.Timestamp.Subtract(timestamp).Days > 30).ToArray();
                Log.Logger.Information($"Found {old.Length} older than 30 days geo data.");

                foreach (var geoToSave in nonexistent.Union(old).Take(100))
                {
                    var geoFromApi = await _geoApiClient.GetAsync(geoToSave.Latitude, geoToSave.Longitude);
                    if (geoFromApi.Status == "OK")
                    {
                        Log.Logger.Information($"Get geo api OK.");
                        var addressComponents  = geoFromApi.Results[0].AddressComponents;
                        var country = addressComponents.FirstOrDefault(x => x.Types.Any(t => t == "country"))?.ShortName;
                        var streetNo = addressComponents.FirstOrDefault(x => x.Types.Any(t => t == "street_number"))?.ShortName;
                        var street = addressComponents.FirstOrDefault(x => x.Types.Any(t => t == "route"))?.ShortName;
                var city = addressComponents.FirstOrDefault(x => x.Types.Any(t => t == "postal_town"))?.ShortName ??
                                   addressComponents.FirstOrDefault(x => x.Types.Any(t => t == "locality"))?.ShortName;
                       
                        var postalNo = addressComponents.FirstOrDefault(x => x.Types.Any(t => t == "postal_code"))?.ShortName;
                        var formattedaddress = geoFromApi.Results[0].FormattedAddress;
                        var geoDto = new StationGeoDto(geoToSave.Longitude,geoToSave.Latitude, geoToSave.StationId,geoToSave.ProviderId,
                           street,streetNo,city,postalNo,null,country, formattedaddress,timestamp);
                        if (!string.IsNullOrEmpty(geoToSave.StationGeoDataId))
                            geoDto.StationGeoDataId = geoToSave.StationGeoDataId;
                        await _smogDatabase.SaveStationGeoAsync(geoDto);
                        Log.Logger.Information($"SaveStationGeoAsync OK.");
                        await Task.Delay(500, CancellationToken.None);
                    }
                   
                }
                Log.Logger.Information($"StationGeoDataService.OnTimerElapsed() stop.");
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, $"Error StationGeoDataService.OnTimerElapsed().");
            }
            finally
            {
                _isRunning = false;
            }
        }
    }
}
