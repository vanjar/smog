// ReSharper disable InconsistentNaming

using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.Model
{
    public class SmogPolutantDto
    {
        public double Value { get; set; }
        public SmogPolutantType Type { get; set; }
        public SmogPolutantInterval Interval { get; set; }
        public SmogPolutantUnit Unit { get; set; }

        public SmogPolutantDto(double value, SmogPolutantType type, SmogPolutantInterval interval, SmogPolutantUnit unit)
        {
            Value = value;
            Type = type;
            Interval = interval;
            Unit = unit;
        }
    }
}