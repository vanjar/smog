using System;
using Marten.Schema;
using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.Model
{
    public class DataProviderInfoDto
    {

        [Identity]
        public string DataProviderInfoId { get; set; }

        public string ProviderId { get; set; }

        public DateTimeOffset LastFetchStart { get; set; }

        public DateTimeOffset? LastFetchStop { get; set; }

        public LastFetchStatus Status { get; set; }

        public string Info { get; set; }

        public DataProviderInfoDto()
        {
            
        }
        
        public static DataProviderInfoDto CreateNew(string providerId, DateTimeOffset lastFetchStart)
        {
            return new DataProviderInfoDto
            {
                ProviderId = providerId,
                LastFetchStart = lastFetchStart,
                Status = LastFetchStatus.Fetching,
                DataProviderInfoId = Guid.NewGuid().ToString("N")
            };
        }

        public void SetFetchEnd(DateTimeOffset lastFetchStop, LastFetchStatus status,
            string info = null)
        {
            LastFetchStop = lastFetchStop;
            Status = status;
            Info = info;
        }

    }
}