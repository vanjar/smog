namespace Smog.Infrastructure.Model.Dictionaries
{
    public class SmogPolutantTypeDescription
    {
        public string ShortName { get;  }
        public string LongName { get;  }
        public string Description { get;  }

        public SmogPolutantTypeDescription(string shortName, string longName, string description)
        {
            ShortName = shortName;
            LongName = longName;
            Description = description;
        }

        public static SmogPolutantTypeDescription Unknown 
            => new SmogPolutantTypeDescription("unknown", "unknown", null);
    }
}