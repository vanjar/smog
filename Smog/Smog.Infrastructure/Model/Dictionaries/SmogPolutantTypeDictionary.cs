using System.Collections.Generic;
using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.Model.Dictionaries
{
    public static class SmogPolutantTypeDictionary
    {
        private static  readonly Dictionary<SmogPolutantType, SmogPolutantTypeDescription> _dict
            = new Dictionary<SmogPolutantType, SmogPolutantTypeDescription>()
            {
                {SmogPolutantType.SO2, new SmogPolutantTypeDescription("SO\u2082", "Sulfur dioxide", null)},
                {SmogPolutantType.O3, new SmogPolutantTypeDescription("O\u2083", "Ozone", null)},
                {SmogPolutantType.CO, new SmogPolutantTypeDescription("CO", "Carbon monoxide", null)},
                {SmogPolutantType.NO2, new SmogPolutantTypeDescription("NO\u2082", "Nitrogen dioxide", null)},
                {SmogPolutantType.Pm10, new SmogPolutantTypeDescription("PM\u2081\u2080", "Particulate matter 2.5-10\u00B5m", null)},
                {SmogPolutantType.H2S, new SmogPolutantTypeDescription("H\u2082S", "Hydrogen sulfide", null)},
                {SmogPolutantType.Benzene, new SmogPolutantTypeDescription("C\u2086H\u2086", "Benzene", null)},
                {SmogPolutantType.Methane, new SmogPolutantTypeDescription("CH\u2084", "Methane", null)},
                {SmogPolutantType.NH3, new SmogPolutantTypeDescription("NH\u2083", "Ammonia", null)},
                {SmogPolutantType.Pm2Point5, new SmogPolutantTypeDescription("PM\u2082\u22C5\u2085", "Particulate matter <2.5\u00B5m", null)},
            };

        public static SmogPolutantTypeDescription Get(SmogPolutantType type)
        {
            SmogPolutantTypeDescription description;
            return !_dict.TryGetValue(type,out description) ? SmogPolutantTypeDescription.Unknown : description;
        }

      
    }
}