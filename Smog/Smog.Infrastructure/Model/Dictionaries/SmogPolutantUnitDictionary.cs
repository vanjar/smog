using System.Collections.Generic;
using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.Model.Dictionaries
{
    public static class SmogPolutantUnitDictionary
    {
        private static readonly Dictionary<SmogPolutantUnit, string> _dict
            = new Dictionary<SmogPolutantUnit, string>()
            {
                {SmogPolutantUnit.MikroGramsPerCubicMetre, "\u00B5g/m\u00B3"},
                {SmogPolutantUnit.MiliGramsPerCubicMetre, "mg/m\u00B3"}
            };

        public static string Get(SmogPolutantUnit type)
        {
            string description;
            return !_dict.TryGetValue(type, out description) ? "unknown" : description;
        }
    }
}