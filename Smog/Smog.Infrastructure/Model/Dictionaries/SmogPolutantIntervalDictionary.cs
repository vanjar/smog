using System.Collections.Generic;
using Smog.Infrastructure.Model.Enums;

namespace Smog.Infrastructure.Model.Dictionaries
{
    public static class SmogPolutantIntervalDictionary
    {
        private static readonly Dictionary<SmogPolutantInterval, string> _dict
            = new Dictionary<SmogPolutantInterval, string>()
            {
                {SmogPolutantInterval.Hour, "1h"},
                {SmogPolutantInterval.EightHour, "8h"},
                {SmogPolutantInterval.TwentyFourHour, "24h"}
            };

        public static string Get(SmogPolutantInterval type)
        {
            string description;
            return !_dict.TryGetValue(type, out description) ? "unknown" : description;
        }
    }
}