﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marten.Schema;

namespace Smog.Infrastructure.Model
{
    public class StationGeoDto
    {
        [Identity]
        public string StationGeoDataId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string StationId { get; set; }
        public string ProviderId { get; set; }

        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string City { get; set; }
        public string PostalNo { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public string Status { get; set; }
        public string FormattedAddress { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string Key => StationKey.Create(StationId, ProviderId);
        public StationGeoDto()
        {
            
        }

        public StationGeoDto(double longitude, double latitude, string stationId,string providerId, string street, string streetNo, string city,string postalNo,
            string state, string country,string formattedAddress, DateTimeOffset timestamp)
        {
            StationGeoDataId =Guid.NewGuid().ToString("N");
            Longitude = longitude;
            Latitude = latitude;
            StationId = stationId;
            Street = street;
            StreetNo = streetNo;
            PostalNo = postalNo;
            City = city;
            State = state;
            Country = country;
            Status = "OK";
            FormattedAddress = formattedAddress;
            Timestamp = timestamp;
            ProviderId = providerId;
        }

        public StationGeoDto(double longitude, double latitude, string stationId,string providerId, DateTimeOffset timestamp)
        {
            StationGeoDataId = Guid.NewGuid().ToString("N");
            Longitude = longitude;
            Latitude = latitude;
            StationId = stationId;
            ProviderId = providerId;
            Timestamp = timestamp;
        }
    }
}
