using System;
using System.Collections.Generic;
using Marten.Schema;
// ReSharper disable InconsistentNaming
namespace Smog.Infrastructure.Model
{
    public static class StationKey
    {
        public static string Create(string stationId, string providerId)
        {
            if (stationId.Contains("_") || providerId.Contains("_"))
                throw new ArgumentException("_ is not valid char for station key");
            return $"{stationId}_{providerId}";
        }

        public static string GetStationId(string key)
        {
            return key.Split('_')[0];
        }

        public static string GetProviderId(string key)
        {
            return key.Split('_')[1];
        }
    }

    public class SmogDto
    {
        [Identity]
        public string SmogDataId { get; set; }
        public DateTime FetchTime { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string StationName { get; set; }
        public string StationId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public double Altitude { get; set; }
        public string Country { get; set; }
        public string ProviderId { get; set; } 
        public SmogPolutantDto[] Polutants { get; set; }
        public string ResponseHash { get; set; }
        public DateTimeOffset IntervalStart { get; set; } //utc
        public DateTimeOffset IntervalEnd { get; set; } //utc
      
        public string TimeZoneInfoId { get; set; }

        public string Key => StationKey.Create(StationId, ProviderId);

        
        public SmogDto()
        {

        }
        
        public SmogDto(DateTime fetchTime, DateTime? createdTime, string stationName, 
           string stationId, double longitude, double latitude, double altitude, 
           string country, string providerId, string responseHash, DateTimeOffset intervalStart, DateTimeOffset intervalEnd,
           string timeZoneInfoId,params SmogPolutantDto[] polutants)
       {

           FetchTime = fetchTime;
           CreatedTime = createdTime;
           StationName = stationName;
           StationId = stationId;
           Longitude = longitude;
           Latitude = latitude;
           Altitude = altitude;
           Country = country;
           ProviderId = providerId;
           Polutants = polutants;
           ResponseHash = responseHash;
           IntervalStart = intervalStart.KeepOnlyHoursAndMinutes();
           IntervalEnd = intervalEnd.KeepOnlyHoursAndMinutes();
           SmogDataId = Guid.NewGuid().ToString("N");
           TimeZoneInfoId = timeZoneInfoId;
       }
    }

    
}