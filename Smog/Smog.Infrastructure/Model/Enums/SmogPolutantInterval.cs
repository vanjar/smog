namespace Smog.Infrastructure.Model.Enums
{
    public enum SmogPolutantInterval
    {
        Hour = 1,
        EightHour = 2,
        TwentyFourHour = 3
    }
}