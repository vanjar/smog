namespace Smog.Infrastructure.Model
{
    public enum LastFetchStatus
    {
        Fetching = 0,
        Ok = 1,
        Warning = 2,
        Error = 3
    }
}