namespace Smog.Infrastructure.Model.Enums
{
    public enum SmogPolutantType
    {
        SO2 = 1,
        O3 = 2,
        CO = 3,
        NO2 = 4,
        Pm10 = 5,
        H2S = 6,
        Benzene = 7,
        Methane = 8,
        NH3 = 9,
        Pm2Point5 = 10
    }
}