namespace Smog.Infrastructure.Model.Enums
{
    public enum SmogPolutantUnit
    {
        MikroGramsPerCubicMetre = 0,
        MiliGramsPerCubicMetre = 1
    }
}