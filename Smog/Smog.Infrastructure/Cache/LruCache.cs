﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smog.Infrastructure.Cache
{
    public class LruCache<TKey, TValue>
    {
        private class LruCacheNode
        {
            public readonly TKey Key;
            public TValue Value;

            public LruCacheNode(TKey key, TValue value)
            {
                Key = key;
                Value = value;
            }

            public virtual LruCacheNode Clone()
            {
                return new LruCacheNode(Key, Value);
            }
        }

        private class LruCacheNodeWithTime : LruCacheNode
        {
            public readonly DateTime CreatedTime;

            public LruCacheNodeWithTime(TKey key, TValue value)
                : base(key, value)
            {
                CreatedTime = DateTime.Now;
            }

            public override LruCacheNode Clone()
            {
                return new LruCacheNodeWithTime(Key, Value);
            }
        }

        private readonly int? _maxItems;
        private readonly TimeSpan? _maxAge;
        private readonly int? _maxCapacity;
        private readonly Func<TValue, int> _getCapacity;
        private readonly Dictionary<TKey, LinkedListNode<LruCacheNode>> _dictionary;
        private readonly LinkedList<LruCacheNode> _linkedList;

        private readonly object _syncRoot = new object();

        public int Capacity { get; private set; }

        public LruCache(int? maxItems = null, TimeSpan? maxAge = null, int? maxCapacity = null, Func<TValue, int> getCapacity = null)
        {
            _maxItems = maxItems;
            _maxAge = maxAge;
            _maxCapacity = maxCapacity;
            _getCapacity = getCapacity;
            _dictionary = new Dictionary<TKey, LinkedListNode<LruCacheNode>>();
            _linkedList = new LinkedList<LruCacheNode>();
        }

        public LruCache<TKey, TValue> Clone()
        {
            lock (_syncRoot)
            {
                var clone = new LruCache<TKey, TValue>(_maxItems, _maxAge, _maxCapacity, _getCapacity);
                foreach (LruCacheNode node in _linkedList)
                {
                    LinkedListNode<LruCacheNode> linkedListNode = clone._linkedList.AddLast(node.Clone());
                    clone._dictionary[node.Key] = linkedListNode;
                }
                return clone;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                return GetValueOrDefault(key);
            }
            set
            {
                lock (_syncRoot)
                {
                    LruCacheNode node = GetNode(key);
                    if (node == null)
                    {
                        Add(key, value);
                    }
                    else
                    {
                        Capacity -= GetCapacity(node.Value);
                        node.Value = value;
                        Capacity += GetCapacity(node.Value);
                    }
                }
            }
        }

        private int GetCapacity(TValue value)
        {
            return _getCapacity?.Invoke(value) ?? 1;
        }

        public TKey[] GetKeys(Func<TKey, TValue, bool> filter = null)
        {
            lock (_syncRoot)
            {
                return _linkedList.Where(n => filter == null || filter(n.Key, n.Value)).Select(n => n.Key).ToArray();
            }
        }

        public TValue[] GetValues()
        {
            lock (_syncRoot)
            {
                return _linkedList.Select(n => n.Value).ToArray();
            }
        }

        public void Remove(TKey key)
        {
            lock (_syncRoot)
            {
                LinkedListNode<LruCacheNode> node;
                if (_dictionary.TryGetValue(key, out node))
                    Remove(node);
            }
        }

        public void Remove(Func<TKey, TValue, bool> filter)
        {
            if (filter == null)
                throw new ArgumentNullException(nameof(filter));
            lock (_syncRoot)
            {
                GetKeys(filter).Each(Remove);
            }
        }

        public void Add(TKey key, TValue value)
        {
            lock (_syncRoot)
            {
                if (_dictionary.ContainsKey(key))
                    throw new ArgumentException("Key already exists.");
                LinkedListNode<LruCacheNode> newNode = _linkedList.AddFirst(_maxAge == null ? new LruCacheNode(key, value) : new LruCacheNodeWithTime(key, value));
                _dictionary.Add(key, newNode);
                Capacity += GetCapacity(newNode.Value.Value);
                if (_maxItems != null && _linkedList.Count > _maxItems)
                {
                    Remove(_linkedList.Last);
                }
                while (_maxCapacity != null && Capacity > _maxCapacity)
                {
                    Remove(_linkedList.Last);
                }
            }
        }

        public bool ContainsKey(TKey key)
        {
            lock (_syncRoot)
            {
                return _dictionary.ContainsKey(key);
            }
        }

        public TValue GetValueOrDefault(TKey key)
        {
            TValue value;
            TryGetValue(key, out value);
            return value;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            lock (_syncRoot)
            {
                LruCacheNode node = GetNode(key);
                if (node != null)
                {
                    value = node.Value;
                    return true;
                }
                value = default(TValue);
                return false;
            }
        }

        private LruCacheNode GetNode(TKey key)
        {
            LinkedListNode<LruCacheNode> node;
            if (_dictionary.TryGetValue(key, out node))
            {
                if (_maxAge != null && DateTime.Now - ((LruCacheNodeWithTime)node.Value).CreatedTime > _maxAge)
                {
                    Remove(node);
                    return null;
                }
                if (_linkedList.First != node)
                {
                    _linkedList.Remove(node);
                    _linkedList.AddFirst(node);
                }
                return node.Value;
            }
            return null;
        }

        private void Remove(LinkedListNode<LruCacheNode> node)
        {
            _dictionary.Remove(node.Value.Key);
            _linkedList.Remove(node);
            Capacity -= GetCapacity(node.Value.Value);
        }

        public void Clear()
        {
            lock (_syncRoot)
            {
                _dictionary.Clear();
                _linkedList.Clear();
                Capacity = 0;
            }
        }
    }
}
