﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Smog.Infrastructure.Model;

namespace Smog.Infrastructure.Cache
{
    public class SmogCache
    {
        
        private readonly LruCache<string, List<SmogDto>> _smogDataByStation=
           new LruCache<string, List<SmogDto>>(10000, TimeSpan.FromDays(1));

        private readonly LruCache<string, StationGeoDto> _geoDataByStation =
           new LruCache<string, StationGeoDto>(10000, TimeSpan.FromDays(1));
        
        public void UpdateSmogData(string key, List<SmogDto> smogDtos)
        {
            _smogDataByStation[key] = smogDtos;
        }

        public Dictionary<string, List<SmogDto>> GetSmogData(string[] keys)
        {
            var result = new Dictionary<string, List<SmogDto>>();
            foreach (var key in keys)
            {
                if (_smogDataByStation.ContainsKey(key))
                    result.Add(key,_smogDataByStation[key]);
            }
            return result;
        }

        public Dictionary<string,List<SmogDto>> GetSmogData()
        {
            string[] keys = _smogDataByStation.GetKeys();
            return
                keys.Select(k => new KeyValuePair<string, List<SmogDto>>(k, _smogDataByStation[k]))
                    .ToDictionary(x => x.Key, x => x.Value);
        }

        public List<SmogDto> GetSmogData(string key)
        {
            return _smogDataByStation[key];
        }

        public StationGeoDto GetStationGeoData(string key)
        {
            return _geoDataByStation[key];
        }

        public List<StationGeoDto> GetStationGeoData(string[] keys)
        {
            List<StationGeoDto> stationGeoDtos = new List<StationGeoDto>();
            foreach (var key in keys)
            {
                if (_geoDataByStation.ContainsKey(key))
                    stationGeoDtos.Add(_geoDataByStation[key]);
            }
            return stationGeoDtos;
        }

        public List<StationGeoDto> GetStationGeoData()
        {
            return _geoDataByStation.GetValues().ToList();

        }

        public void UpdateStationGeoData(string key, StationGeoDto geoDto)
        {
            _geoDataByStation[key] = geoDto;
        }

        public void Clear()
        {
            _smogDataByStation.Clear();
            _geoDataByStation.Clear();
        }
    }
}
